(function ($, root, undefined) {
	$(function () {
		'use strict';

		const navWrapper = $('.site-nav');
		const navBurger = $('.burger-menu');
		const body = $('body');

		navBurger.on('click', function () {
			body.toggleClass('is-menu-open');
			navWrapper.toggleClass('is-active');
		});

		/* Accordions */
		const aboutUsAccordionBtn = $('.about-us-accordion-btn');
		const aboutUsAccordionElement = $('.about-us-accordion-element');
		const accordionBtn = $('.accordion-title-wrapper');
		const accordionElement = $('.accordion-element');

		function accordionFunction(btn, element) {
			btn.on('click', function () {
				const index = btn.index(this);

				element.removeClass('is-active');
				$(element[index]).addClass('is-active');

				if (window.innerWidth < 768) {
					$('html, body').animate({
						scrollTop: $(this).offset().top - 55,
					}, 500);
				}
			});
		}

		accordionFunction(aboutUsAccordionBtn, aboutUsAccordionElement);
		accordionFunction(accordionBtn, accordionElement);

		/* Facts Slider */
		let factsSlideIndex = 0;
		const factsPrevSlide = $('.facts-prev-btn');
		const factsNextSlide = $('.facts-next-btn');
		const factsPagination = $('.facts-slider-page');

		function mod(n, m) {
			return ((n % m) + m) % m;
		}

		function factsSlides(n) {
			const slides = $('.fact-slide');

			factsSlideIndex = mod(n, slides.length);

			slides.removeClass('is-active');
			factsPagination.removeClass('is-active');
			factsNextSlide.removeClass('is-hidden');
			factsPrevSlide.removeClass('is-hidden');

			if (window.innerWidth >= 1024 && factsSlideIndex >= slides.length - 1) {
				factsNextSlide.addClass('is-hidden');
			}
			if (window.innerWidth >= 1024 && factsSlideIndex <= 0) {
				factsPrevSlide.addClass('is-hidden');
			}

			$(slides[factsSlideIndex]).addClass('is-active');
			$(factsPagination[factsSlideIndex]).addClass('is-active');
		}

		factsSlides(0);

		factsPagination.on('click', function () {
			const index = factsPagination.index(this);

			factsPagination.removeClass('is-active');
			$(factsPagination[index]).addClass('is-active');

			factsSlideIndex = index;
			factsSlides(factsSlideIndex);
		});

		factsPrevSlide.on('click', function () {
			factsSlides(factsSlideIndex - 1);
		});

		factsNextSlide.on('click', function () {
			factsSlides(factsSlideIndex + 1);
		});

		/* Homepage Slides */
		const homePrevSlide = $('.homePrev');
		const homeNextSlide = $('.homeNext');
		let homepageSlideIndex = 1;

		function showHomeSlides(n) {
			let i;
			const slides = $('.testimonial-item');
			if (n > slides.length) {
				homepageSlideIndex = 1
			}
			if (n < 1) {
				homepageSlideIndex = slides.length
			}
			for (i = 0; i < slides.length; i++) {
				$(slides[i]).removeClass('is-active');
			}
			$(slides[homepageSlideIndex - 1]).addClass('is-active');
		}

		showHomeSlides(homepageSlideIndex);

		function plusHomeSlides(n) {
			showHomeSlides(homepageSlideIndex += n);
		}

		homePrevSlide.on('click', function () {
			plusHomeSlides(-1)
		});

		homeNextSlide.on('click', function () {
			plusHomeSlides(1)
		});

		/* Car Slider */
		let carSliderIndex = 0;
		const dateBtn = $('.timeline-item');
		const dateBtnWidth = parseFloat(dateBtn.css('width'));
		const carNextSlide = $('.timeline-control.next');
		const carPrevSlide = $('.timeline-control.prev');
		const carSlide = $('.timeline-slide');
		const car = $('.timeline-car');
		const carLength = parseFloat(car.css('width'));
		let initialCarPosition = parseFloat($(dateBtn[carSliderIndex]).css('left'));
		const road = $('.timeline-wrapper');

		function timelineSlider(element) {
			element.removeClass('is-active');
			$(element[carSliderIndex]).addClass('is-active');
		}

		function carSliderControl(num) {
			carSliderIndex = mod(num, dateBtn.length);
		}

		function setCarPosition() {
			const position = parseFloat($(dateBtn[carSliderIndex]).css('left'));
			const routeToGo = Math.abs(position - initialCarPosition);
			const timing = (parseInt(routeToGo / 2).toFixed()) * 8;

			car.transition({
				x: position - carLength + dateBtnWidth,
			}, timing, 'ease');

			if (window.innerWidth < 1360) {
				road.animate({
					scrollLeft: position - carLength + dateBtnWidth,
				}, timing);
			}

			initialCarPosition = position;
		}

		setCarPosition();

		dateBtn.on('click', function () {
			const index = dateBtn.index(this);
			carSliderIndex = index;

			timelineSlider(dateBtn);
			timelineSlider(carSlide);

			setCarPosition();
		});

		carNextSlide.on('click', function () {
			carSliderControl(carSliderIndex + 1);

			timelineSlider(dateBtn);
			timelineSlider(carSlide);

			setCarPosition();
		});

		carPrevSlide.on('click', function () {
			carSliderControl(carSliderIndex - 1);

			timelineSlider(dateBtn);
			timelineSlider(carSlide);

			setCarPosition();
		});

		/* Benefits Slick */
		$('.benefit-slider').slick({
			dots: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 500,
			slidesToShow: 2,
			slidesToScroll: 2,
			prevArrow: $('.benefitsPrev'),
			nextArrow: $('.benefitsNext'),
			responsive: [
				{
					breakpoint: 1023,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					},
				},
				{
					breakpoint: 540,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
					},
				},
			],
		});

		$('.benefits-icons').slick({
			mobileFirst: true,
			dots: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 500,
			prevArrow: $('.jobHubPrev'),
			nextArrow: $('.jobHubNext'),
			slidesToShow: 2,
			slidesToScroll: 2,
			responsive: [
				{
					breakpoint: 560,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					},
				},
				{
					breakpoint: 1023,
					settings: 'unslick',
				},
			],
		});

		/* We offer slider */
		$('.we-offer-slider').slick({
			mobileFirst: true,
			dots: false,
			infinite: false,
			autoplay: false,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: $('.prev-offer'),
			nextArrow: $('.next-offer'),
			responsive: [
				{
					breakpoint: 549,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					},
				},
				{
					breakpoint: 1023,
					settings: 'unslick',
				},
			],
		});

		/* Our Vision Slick */
		$('.vision-list').slick({
			mobileFirst: true,
			dots: false,
			infinite: false,
			autoplay: false,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: $('.prev-vision'),
			nextArrow: $('.next-vision'),
			responsive: [
				{
					breakpoint: 510,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
					},
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					},
				},
				{
					breakpoint: 1023,
					settings: 'unslick',
				},
			],
		})
	});

	// Apply
	const applyBtn = $('.apply-btn');
	const closeModalBtn = $('.close-btn');
	const modalContainer = $('.has-modal');
	const accordionBtn = $('.accordion-btn');

	accordionBtn.on('click', function () {
		modalContainer.addClass('is-active');
		$('html, body').animate({scrollTop: 0}, 500);
	});

	applyBtn.on('click', function () {
		modalContainer.addClass('is-active');
		$('html, body').animate({scrollTop: 0}, 500);
	});

	closeModalBtn.on('click', function () {
		modalContainer.removeClass('is-active');
	});

	// Expand
	const expandBtn = $('.expand-btn');
	const expandWrapper = $('.hidden-text-wrapper');

	expandBtn.on('click', function () {
		expandWrapper.toggleClass('is-shown');
	});

	// Video modal
	const openModal = $('.video-modal-btn');
	const closeModal = $('.close-video-modal');
	const videoModal = $('.video-modal');
	const modalClass = 'modal-container';
	const youtubeObject = {
		classes: ['is-about-us', 'is-meet-us'],
		links: ['https://www.youtube.com/embed/kTGcoFlipc8', 'https://www.youtube.com/embed/uQOT1JWMLWU'],
	}

	function YoutubeVideo(link, className) {
		return `
			<div class="${className}">
				<iframe 
					class="youtube-vid"
					width="560" 
					height="315" 
					src="${link}?autoplay=1&showinfo=0&controls=0&autohide=1" 
					frameborder="0" 
					allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
					allowfullscreen>
				</iframe>
			</div>
		`;
	};

	function includesPolyfill() {
		if (!String.prototype.includes) {
			Object.defineProperty(String.prototype, 'includes', {
				value: function(search, start) {
					if (typeof start !== 'number') {
						start = 0;
					}

					return (start + search.length > this.length) ?  false :  this.indexOf(search, start) !== -1;
				}
			})
		}
	}
	
	function removeModal() {
		$('body').css('overflow', 'auto');
		videoModal.removeClass('is-active');
		$(`.${modalClass}`).remove();
	}
	
	function addModal(e) {
		$('body').css('overflow', 'hidden');
		chooseLink(e);
		videoModal.addClass('is-active');
	}
	
	function chooseLink(e) {
		youtubeObject.classes.forEach( (el, i) => {
			if(e.target.className.includes(el)) {
				videoModal.append(YoutubeVideo(youtubeObject.links[i], modalClass));
			}
		});
	}
	
	includesPolyfill();

	openModal.on('click', function(e) {
		e.preventDefault();
		addModal(e);
	});

	closeModal.on('click', function(e) {
		e.preventDefault();
		removeModal(e);
	});

	$(window).on('click', function(e) {
		(e.target.className.includes('video-modal is-active')) && removeModal();
	});

	// Searchform
	$('#state').on('change', function (evt) {
		let selector = $(this).val();
		$('#town > option').hide();
		$('#town > option').filter(function () {
			return $(this).data('pub') == selector
		}).show();
	});

	// File name
	$('#add_cv').change(function() {
		const CvFilename = this.files[0].name;
		const CvOverlay = $('.cv-overlay');
		CvOverlay.text(CvFilename);
	});

	$('#other_docs').change(function() {
		const DocsFilename = this.files[0].name;
		const DocsOverlay = $('.docs-overlay');
		DocsOverlay.text(DocsFilename);
	});
})(jQuery, this);
