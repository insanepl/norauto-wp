<?php /* Template Name: Meet Us Template */ get_header(); ?>
	<div class="content-container">
      <div class="meet-us-hero">
        <div class="meet-us-vid-wrapper">
          <video autoplay muted loop class="meet-hero-vid">
            <source src="<?php echo get_template_directory_uri(); ?>/assets/vid/poznaj_nas.ogg" type="video/ogg" />
            <source src="<?php echo get_template_directory_uri(); ?>/assets/vid/poznaj_nas.mp4" type="video/mp4" />
          </video>
          <div class="meet-us-more-wrapper">
            <button class="see-more-btn video-modal-btn is-meet-us">Zobacz cały film</button>
          </div>
        </div>
        <div class="meet-hero-section">
          <h1 class="meet-hero-header">
            Poznaj nas <strong>bliżej</strong>
          </h1>
          <h3 class="meet-hero-subheader">
            <span class="meet-hero-subitem">
              Ciesz się drogą
            </span>
            <span class="meet-hero-subitem">
              Rys historyczny
            </span>
            <span class="meet-hero-subitem">
              Najważniejsze wydarzenia
            </span>
            <span class="meet-hero-subitem">
              Ciekawostki
            </span>
          </h3>
        </div>
      </div>
      <div class="vision-section">
          <div class="container">
            <div class="vision-text-wrapper">
              <h2 class="vision-header">
                <span>
                  Ciesz się drogą
                </span>
                Wybieraj proste rozwiązania
              </h2>
            </div>
            <div class="vision-slider-wrapper">
              <button class="slider-btn prev-btn prev-vision">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
                  <path
                    d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"/>
                </svg>
              </button>
              <ul class="vision-list">
                <li class="vision-list-element">
                  <div class="vision-img-wrapper">
                    <img
                    	src="<?php echo get_template_directory_uri(); ?>/assets/img/lightbulb.png"
                    	alt="Proponować proste rozwiazania"
                    	class="vision-img"
					          />
                  </div>
                  <h3 class="vision-title">
                    Proponować proste <br/>
                    rozwiązania
                  </h3>
                  <p class="vision-text">
                    Szeroka, przejrzysta i dostępna oferta odpowiadająca na potrzeby naszych klientów. </p>
                </li>
                <li class="vision-list-element">
                  <div class="vision-img-wrapper">
                    <img
                    	src="<?php echo get_template_directory_uri(); ?>/assets/img/create.png"
                    	alt="Tworzyć przyjazną firmę"
                    	class="vision-img"
					          />
                  </div>
                  <h3 class="vision-title">
                    Tworzyć przyjazną <br/>
                    firmę
                  </h3>
                  <p class="vision-text">
                    Przyjazna, spersonalizowana i profesjonalna obsługa klienta.
                  </p>
                </li>
                <li class="vision-list-element">
                  <div class="vision-img-wrapper">
                    <img
                    	src="<?php echo get_template_directory_uri(); ?>/assets/img/change.png"
                    	alt="Zmieniać naszą firmę"
                    	class="vision-img"
					          />
                  </div>
                  <h3 class="vision-title">
                    Zmieniać naszą <br/>
                    firmę
                  </h3>
                  <p class="vision-text">
                    Szybka i elastyczna firma, w której korzystamy z nowoczesnych rozwiązań technologicznych.
                  </p>
                </li>
                <li class="vision-list-element">
                  <div class="vision-img-wrapper">
                    <img
                    	src="<?php echo get_template_directory_uri(); ?>/assets/img/collaboration.png"
                    	alt="Działać razem"
                    	class="vision-img"
					          />
                  </div>
                  <h3 class="vision-title">
                    Działać <br/>
                    razem
                  </h3>
                  <p class="vision-text">
                    Kultura organizacyjna i praktyki menadżerskie zorientowane na stworzenie przyjaznego miejsca pracy.
                  </p>
                </li>
              </ul>
              <button class="slider-btn next-btn next-vision">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
                  <path
                    d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"/>
                </svg>
              </button>
            </div>
          </div>
      </div>

      <div class="history-section">
        <div class="history-content">
          <p class="history-title">
            Rys historyczny
          </p>
          <p class="history-header">
            W Polsce Norauto działa od 1997 roku.
          </p>
          <p class="history-text">
            Nasze początki sięgają 1970 r. Właśnie wtedy, we francuskim mieście Lille powstało pierwsze centrum Norauto na świecie, wprowadzające stosowany do dzisiaj koncept połączenia sklepu motoryzacyjnego oraz serwisu samochodów osobowych. <br /> <br />
            W Polsce Norauto działa od 1997 roku, a pierwsze centrum otwarto w 1998 roku. <br /> <br />
            Dziś zarządzamy 39 placówkami rozlokowanymi w największych polskich miastach.
          </p>
        </div>
      </div>

      <div class="norauto-timeline">
        <div class="timeline-road">
          <div class="timeline-wrapper">
            <div class="timeline-gray-place">
              <div class="timeline-milestones">
                <div class="timeline-milestone">
                  <p class="milestone-details">
                    Powstanie i rozwój sieci Norauto
                  </p>
                  <p class="milestone-title">
                    Powstanie
                  </p>
                </div>
                <div class="timeline-milestone">
                  <p class="milestone-details">
                    Dywersyfikacja sieci
                  </p>
                  <p class="milestone-title">
                    Dywersyfikacja
                  </p>
                </div>
                <div class="timeline-milestone">
                  <p class="milestone-details">
                    Mobilność i innowacje
                  </p>
                  <p class="milestone-title">
                    Mobilność
                  </p>
                </div>
              </div>
            </div>

            <div class="timeline-main-road">
              <div class="timeline-events">
                <button class="timeline-item is-important is-active" data-year="1970" style="left: 3%"></button>
								<button class="timeline-item y1986" data-year="1986" style="left: 8.7%"></button>
								<button class="timeline-item y1986" data-year="1986" style="left: 8.7%"></button>
                <button class="timeline-item" data-year="1990" style="left: 12%"></button>
                <button class="timeline-item" data-year="1992" style="left: 14%"></button>
                <button class="timeline-item" data-year="1996" style="left: 17.3%"></button>
                <button class="timeline-item is-important" data-year="1998" style="left: 19.3%"></button>
                <button class="timeline-item is-important" data-year="2002" style="left: 40.2%"></button>
                <button class="timeline-item" data-year="2003" style="left: 42.4%"></button>
                <button class="timeline-item" data-year="2004" style="left: 44.8%"></button>
                <button class="timeline-item" data-year="2005" style="left: 47.2%"></button>
                <button class="timeline-item" data-year="2007" style="left: 52.5%"></button>
                <button class="timeline-item" data-year="2008" style="left: 54.8%"></button>
                <button class="timeline-item is-important" data-year="2009" style="left: 57.1%"></button>
                <button class="timeline-item is-important" data-year="2010" style="left: 75.3%"></button>
                <button class="timeline-item" data-year="2011" style="left: 77.6%"></button>
                <button class="timeline-item" data-year="2014" style="left: 83.3%"></button>
                <button class="timeline-item" data-year="2015" style="left: 85.3%"></button>
                <button class="timeline-item" data-year="2016" style="left: 87.3%"></button>
								<button class="timeline-item y2017" data-year="2017" style="left: 89.3%"></button>
								<button class="timeline-item y2017" data-year="2017" style="left: 89.3%"></button>
								<button class="timeline-item y2017" data-year="2017" style="left: 89.3%"></button>
                <button class="timeline-item is-important" data-year="2018" style="left: 91.6%"></button>
              </div>

              <img
              	class="timeline-car"
              	src="<?php echo get_template_directory_uri(); ?>/assets/img/car.svg"
              	role="presentation"
			          />
            </div>
          </div>
        </div>
        <div class="timeline-details">
          <ul class="timeline-slides">
            <li class="timeline-slide is-active">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1970.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  1970
                </p>
                <p class="timeline-slide-events">
                  Powstanie pierwszego centrum Norauto w Lille, Francja
                </p>
              </div>
            </li>
						<li class="timeline-slide">
							<img
								class="timeline-slide-img"
								src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1986-1.jpg"
								alt="Description"
							/>
							<div class="timeline-slide-content">
								<p class="timeline-slide-year">
									1986
								</p>
								<p class="timeline-slide-events">
									Otwarcie pierwszego centrum Norauto poza granicami Francji - Valencia Alboraya w Hiszpanii
								</p>
							</div>
						</li>
						<li class="timeline-slide">
							<img
								class="timeline-slide-img"
								src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1986-2.jpg"
								alt="Description"
							/>
							<div class="timeline-slide-content">
								<p class="timeline-slide-year">
									1986
								</p>
								<p class="timeline-slide-events">
									W sklepach Norauto pojawiają się produkty na wyłączność: pierwszym produktem jest olej Aliance, po nim przychodzi pora na akumulatory Viking, sukces sprzedaży przeciera szlaki do stworzenia marki Norauto, pod którą sprzedawane będą produkty renomowanych marek motoryzacyjnych
								</p>
							</div>
						</li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1990.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  1990
                </p>
                <p class="timeline-slide-events">
                  Powstanie pierwszego centrum Norauto we Włoszech, w Turynie
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1992.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  1992
                </p>
                <p class="timeline-slide-events">
                  Otwarcie pierwszego centrum Norauto w Belgii, w La Louvrière
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1996.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  1996
                </p>
                <p class="timeline-slide-events">
                  Powstanie pierwszego centrum Norauto w Portugalii, w Porto
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/1998.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  1998
                </p>
                <p class="timeline-slide-events">
                  Otwarcie Norauto za oceanem – powstaje pierwsze centrum w Argentynie, w Buenos Aires <br /> <br />
                  Powstanie Norauto Polska i otwarcie pierwszego centrum w Piasecznie pod Warszawą
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2002.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2002
                </p>
                <p class="timeline-slide-events">
                  Przejęcie sieci Auto5
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2003.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2003
                </p>
                <p class="timeline-slide-events">
                  Przejęcie sieci Maxauto
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2004.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2004
                </p>
                <p class="timeline-slide-events">
                  Przejęcie sieci Midas Europa oraz Midas Ameryka Południowa <br /> <br />
                  Biorąc pod uwagę rozwój portfela marek, obecnych już na 3 kontynentach i reprezentujących zróżnicowaną ofertę, Norauto i jej marki przyjmują nazwę Grupy Norauto
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2005.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2005
                </p>
                <p class="timeline-slide-events">
                  Powstaje pierwsze centrum Norauto na Węgrzech, w Budapeszcie <br /> <br />
                  Powstanie Fundacji Norauto, której zadaniem jest dbanie o bezpieczeństwo na drogach oraz ochrona środowiska naturalnego<br /> <br />
									Jednym z działań Fundacji było utworzenie i organizacja Europejskiej Nagrody Bezpieczeństwa Drogowego.
									Celem nagrody jest promowanie wyróżniających się działań społecznych zrealizowanych przez różnego
									rodzaju stowarzyszenia non-profit, na rzecz bezpieczeństwa drogowego.
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2007.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2007
                </p>
                <p class="timeline-slide-events">
									Powstanie pierwszego sklepu Norauto otwartego 24/7 - sklepu internetowego www.norauto.fr.
									W chwili uruchomienia sklep miał w ofercie 3 000 produktów, do 2009 liczba produktów w
									ofercie wzrosła do 10 000.”
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2008.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2008
                </p>
                <p class="timeline-slide-events">
                  Powstanie Atyse, międzynarodowego magazynu organizującego dostawę opon do wszystkich oddziałów Norauto w Europie.
									Magazyn zapewnia regularny i pewny transport produktów kluczowej rodziny wszystkich sklepów Norauto.
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2009.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2009
                </p>
                <p class="timeline-slide-events">
                  Powstaje pierwsze centrum Norauto w Rumunii <br /> <br />
									Wraz z wzrastającym problemem komunikacji samochodowej w miastach, pociągającej za sobą wzrost zanieczyszczenia
									oraz stresu - Norauto rozwija gamę eko-mobilności obejmującą rowery elektryczne, skutery oraz składane rowery.
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2010.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2010
                </p>
                <p class="timeline-slide-events">
									Uwzględniając nasilającą się współpracę i partnerstwo Norauto z firmami oferującymi rozwiązania z
									zakresu mobilności i eko-mobilności – Grupa Norauto podejmuje decyzję o zmianie nazwy na Grupę
									Mobivia, sugerującej rozwiązania z zakresu szeroko pojętej mobilności, gdzie samochód jest
									tylko jedną z możliwości transportu <br /> <br />
                  Powstanie Via ID – funduszu rozwojowego wspierającego firmy i działania z zakresu mobilności i eko-mobilności
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2011.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2011
                </p>
                <p class="timeline-slide-events">
                  Powstanie pierwszego centrum Norauto w Rosji, w Moskwie
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2014.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2014
                </p>
                <p class="timeline-slide-events">
                  Zakup Bythjul.com – skandynawskiego sklepu internetowego specjalizującego się w sprzedaży opon i felg
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2015.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2015
                </p>
                <p class="timeline-slide-events">
                  Zakup Skruvat.com - skandynawskiego sklepu internetowego specjalizującego się w sprzedaży opon, części i akcesoriów samochodowych
                </p>
              </div>
            </li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2016.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2016
                </p>
                <p class="timeline-slide-events">
                  Mobivia dokonuje zakupu A.T.U (Auto-Teile-Unger) – wiodącej sieci warsztatów samochodowych w Niemczech, Austrii i Szwajcarii
                </p>
              </div>
            </li>
						<li class="timeline-slide">
							<img
								class="timeline-slide-img"
								src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2017-1.jpg"
								alt="Description"
							/>
							<div class="timeline-slide-content">
								<p class="timeline-slide-year">
									2017
								</p>
								<p class="timeline-slide-events">
									Grupa Mobivia zmienia nazwę na Mobivia i opracowuje misję wspólną dla wszystkich swoich firm: Making Mobility Easier <br /> <br />
								</p>
							</div>
						</li>
						<li class="timeline-slide">
							<img
								class="timeline-slide-img"
								src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2017-2.jpg"
								alt="Description"
							/>
							<div class="timeline-slide-content">
								<p class="timeline-slide-year">
									2017
								</p>
								<p class="timeline-slide-events">
									Norauto Polska łączy się z siecią Feu Vert Polska, tym samym liczba punktów sieci sprzedaży wzrasta do 39, a liczba pracowników do 700 <br /> <br/>
								</p>
							</div>
						</li>
						<li class="timeline-slide">
							<img
								class="timeline-slide-img"
								src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2017-3.jpg"
								alt="Description"
							/>
							<div class="timeline-slide-content">
								<p class="timeline-slide-year">
									2017
								</p>
								<p class="timeline-slide-events">
									opracowanie wizji Norauto do roku 2026:<br /><br />
									Enjoy the road with smart solutions / Ciesz się drogą – wybieraj proste rozwiązania
								</p>
							</div>
						</li>
            <li class="timeline-slide">
              <img
                class="timeline-slide-img"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline/2018.jpg"
                alt="Description"
              />
              <div class="timeline-slide-content">
                <p class="timeline-slide-year">
                  2018
                </p>
                <p class="timeline-slide-events">
                  Zmiana logo Norauto <br /> <br />
                  Dołącz do nas, twórzmy przyszłość razem! 
                </p>
              </div>
            </li>
          </ul>

          <div class="timeline-slider-controls">
            <button class="timeline-control prev">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
                <path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"></path>
              </svg>
            </button>
            <button class="timeline-control next">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
                <path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"></path>
              </svg>
            </button>
          </div>
        </div>
      </div>

      <div class="facts-section">
        <div class="container facts-container">
          <div class="facts-title-wrapper">
            <h2 class="facts-title">Ciekawostki</h2>
            <div class="facts-pagination-wrapper">
              <button class="facts-btn prev-btn facts-prev-btn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.2 196.3">
                  <path d="M20 196.3l-20-20 78.1-78.1L0 20.1 20 0l98.2 98.2L20 196.3z"/>
                </svg>
              </button>
              <ul class="facts-pagination">
                <li class="facts-pagination-element">
                  <button class="facts-slider-page is-active"></button>
                </li>
                <li class="facts-pagination-element">
                  <button class="facts-slider-page"></button>
                </li>
                <li class="facts-pagination-element">
                  <button class="facts-slider-page"></button>
                </li>
                <li class="facts-pagination-element">
                  <button class="facts-slider-page"></button>
                </li>
                <li class="facts-pagination-element">
                  <button class="facts-slider-page"></button>
                </li>
              </ul>
              <button class="facts-btn next-btn facts-next-btn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.2 196.3">
                  <path d="M20 196.3l-20-20 78.1-78.1L0 20.1 20 0l98.2 98.2L20 196.3z"/>
                </svg>
              </button>
            </div>
          </div>
          <div class="facts-slider-wrapper">
            <button class="facts-btn prev-btn facts-prev-btn">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.2 196.3">
                <path d="M20 196.3l-20-20 78.1-78.1L0 20.1 20 0l98.2 98.2L20 196.3z"/>
              </svg>
            </button>
            <ul class="facts-slider">
              <li class="fact-slide is-active">
                <ul class="fact-slide-wrapper">
                  <li class="fact-element">
                    <h2 class="fact-number">700</h2>
                    <p class="fact-text">pracowników</p>
                  </li>

									
                  
                  <li class="fact-element">
										<h2 class="fact-number">50</h2>
										<p class="fact-text">marek samochodów obsługiwanych w naszych serwisach</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">26%</h2>
										<p class="fact-text">pracowników ma staż pracy powyżej 10 lat</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">70</h2>
										<p class="fact-text">wiek naszego najbardziej doświadczonego pracownika</p>
									</li>
                </ul>
              </li>

              <li class="fact-slide">
                <ul class="fact-slide-wrapper">
									
                  
                  <li class="fact-element">
										<h2 class="fact-number">30%</h2>
										<p class="fact-text">naszych klientów to kobiety</p>
									</li>
                  
                  <li class="fact-element">
										<h2 class="fact-number">2</h2>
										<p class="fact-text"> co tyle minut w sezonie zimowym realizujemy nowe zamówienie internetowe</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">34</h2>
										<p class="fact-text">średnia wieku naszych pracowników</p>
                  </li>
    
                  <li class="fact-element">
										<h2 class="fact-number">50 000</h2>
										<p class="fact-text">artykułów dostępnych na zamówienie</p>
                  </li>
                </ul>
              </li>

              <li class="fact-slide">
                <ul class="fact-slide-wrapper">
                  <li class="fact-element">
										<h2 class="fact-number">4,5/5</h2>
										<p class="fact-text">średnia ocena na Ceneo</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">150</h2>
										<p class="fact-text">usług wykonywanych w naszych serwisach</p>
                  </li>

									<li class="fact-element">
										<h2 class="fact-number">16 000</h2>
										<p class="fact-text">liczba klientów odwiedzających naszą stronę w szczycie sezonu</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">20 000</h2>
										<p class="fact-text">produktów na stronie www</p>
									</li>
                </ul>
              </li>

              <li class="fact-slide">
                <ul class="fact-slide-wrapper">
									<li class="fact-element">
										<h2 class="fact-number">12%</h2>
										<p class="fact-text">kobiet wśród pracowników</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">39</h2>
										<p class="fact-text">oddziałów</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">40</h2>
										<p class="fact-text">szkoleniowców wewnętrznych</p>
                  </li>
                  
                  <li class="fact-element">
										<h2 class="fact-number">87%</h2>
										<p class="fact-text">
											pracowników uważa, że Norauto wyróżnia się świetną atmosferą pracy i
											przyjaznymi relacjami
										</p>
                  </li>
                </ul>
              </li>

              <li class="fact-slide">
                <ul class="fact-slide-wrapper">
									<li class="fact-element">
										<h2 class="fact-number">95%</h2>
										<p class="fact-text">naszych kierowników pochodzi z awansu wewnętrznego</p>
									</li>

									<li class="fact-element">
										<h2 class="fact-number">3</h2>
										<p class="fact-text">
											co tyle minut w sezonie zimowym do naszego serwisu wjeżdża kolejny samochód
										</p>
                  </li>

									<li class="fact-element">
										<h2 class="fact-number">30</h2>
										<p class="fact-text">szkół, z którymi regularnie współpracujemy</p>
                  </li>
                </ul>
              </li>
            </ul>

            <button class="facts-btn next-btn facts-next-btn">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.2 196.3">
                <path d="M20 196.3l-20-20 78.1-78.1L0 20.1 20 0l98.2 98.2L20 196.3z"/>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </div>

    <div class="video-modal">
      <button class="close-video-modal">&times;</button>
    </div>
<?php get_footer(); ?>
