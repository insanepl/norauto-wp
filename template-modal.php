<?php $cat_id = get_cat_ID ('serwis'); ?>
<div class="modal-wrapper">
  <h1 class="modal-title">Formularz aplikacji</h1>
  <button class="close-btn"></button>

  <div class="modal-box">
    <form method="post" name="apply-form" class="apply-form" enctype="multipart/form-data">
      <div class="form-section">
        <div class="form-element is-required">
          <label class="form-label" for="name">Imię i nazwisko:</label>
          <input name="name" class="text-input" type="text" id="name" required />
        </div>
        <div class="form-element is-required">
          <label class="form-label" for="phone_number">Telefon kontaktowy:</label>
          <input name="phone_number" class="text-input" type="tel" id="phone_number" required />
        </div>
        <div class="form-element is-required">
          <label class="form-label" for="email">Adres e-mail:</label>
          <input name="email" class="text-input" type="email" id="email" required />
        </div>
      </div>

      <div class="form-section">
        <div class="form-element is-select">
          <label class="form-label is-position" for="position_select">Stanowisko:</label>
          <div class="select-wrapper is-position">
            <select name="position_select" class="form-select" id="position_select">
							<?php $stanowisko = get_post_meta($post->ID, 'wpcf-poziom-stanowiska', true); ?>
							<?php if (isset($stanowisko)) { ?>
							<option value="<?php echo $stanowisko; ?>"><?php echo $stanowisko; ?></option>
							<?php } ?>
							<option value="" style="display: none;">Wybierz Stanowisko</option>
							<option value="Pomocnik">Pomocnik</option>
							<option value="Pracownik fizyczny">Pracownik fizyczny</option>
							<option value="Asystent">Asystent</option>
							<option value="Specjalista">Specjalista</option>
							<option value="Kierownik">Kierownik</option>
							<option value="Dyrektor">Dyrektor</option>
            </select>
          </div>
        </div>

        <div class="form-element">
          <label class="form-label is-workplace" for="state_select">Miejsce pracy:</label>
          <div class="select-elements-wrapper">
            <div class="select-wrapper is-workplace">
							<?php

							$argsW = array(
								'hide_empty' => 0,
								'parent' => $cat_id
							);

							$categories = get_categories($argsW);

							$selectW = '<select name="workplace_select_county" id="state_select" class="form-select"><option value="0" style="display: none;">Wybierz województwo</option>';
							foreach($categories as $category) {
							$selectW .= '<option id="'.get_cat_ID ($category->slug).'" value="'.$category->cat_name.'">'.$category->cat_name.'</option>';
							$children = get_categories( array('parent' => $category->term_id) );
							}
							$selectW .= '</select>';

							echo $selectW;
							?>
            </div>

            <div class="select-wrapper is-workplace">
              <select name="workplace_select_town" class="form-select" id="town_select">
								<option value="0" style="display: none;">Wybierz Miasto</option>
								<?php
									$args = array(
									'orderby' => 'name',
									'parent' => $cat_id,
									'hide_empty' => 0
								);

									$categories = get_categories( $args ); $first = true;

									foreach ($categories as $category) {
										$category_id = $category->term_id;
										$children = $wpdb->get_results( "SELECT term_id FROM $wpdb->term_taxonomy WHERE parent=$category_id" );
										$no_children = count($children);

										if ($no_children > 0) {
											$args2 = array(
												'orderby' => 'name',
												'parent' => 2,
												'hide_empty' => 0
											);

										$args2["parent"]=$category->term_id;
										$subcategories = get_categories( $args2 );

										foreach ($subcategories as $subcategory) {
											echo '<option value='.$subcategory->cat_name.' data-pub='.$subcategory->parent.'>'.$subcategory->cat_name.'</option>';
										}
									}
									}
								?>
              </select>
            </div>
            <p class="form-text select-text">
              Jeżeli chcesz zobaczyć lokalizacje naszych Centrów i Centrali – przejdź 
              <a href="/">tutaj</a>
            </p>
          </div>
        </div>
      </div>

      <div class="form-section">
        <div class="form-element is-file">
          <label class="form-label" for="add_cv">Dołącz CV:</label>
          <input
						name="add_cv"
						class="form-file"
						type="file"
						accept="application/pdf,application/vnd.ms-excel,application/msword,text/plain,application/rtf,image/png,image/jpeg"
						id="add_cv"
					/>
          <label for="add_cv" class="form-file-overlay-wrapper">
            <span class="cv-overlay file-overlay-name"></span>
            <button type="button" class="file-overlay-btn">Przeglądaj</button>
          </label>
        </div>
        <div class="form-element is-file">
          <label class="form-label" for="other_docs">Dołącz inne dokumenty:</label>
          <input
						name="other_docs"
						class="form-file"
						type="file"
						accept="application/pdf,application/vnd.ms-excel,application/msword,text/plain,application/rtf,image/png,image/jpeg"
						id="other_docs"
					/>
          <label for="other_docs" class="form-file-overlay-wrapper">
            <span class="docs-overlay file-overlay-name"></span>
            <button class="file-overlay-btn">Przeglądaj</button>
          </label>
        </div>
        <div class="form-element is-message">
          <label class="form-label is-message" for="employer_message">Wiadomość dla pracodawcy:</label>
          <textarea name="employer_message" class="form-textarea" id="employer_message" cols="30" rows="7"></textarea>
          <p class="form-text">
            <strong>Jeżeli nie masz CV</strong> - opisz swoje wykształcenie i doświadczenie zawodowe w polu Wiadomość dla pracodawcy.
          </p>
        </div>
      </div>

      <div class="form-section is-regulations">
        <h2 class="form-title">Klauzula informacyjna:</h2>
        <p class="form-text form-rules-text">
          Wyrażam zgodę na przetwarzanie moich danych osobowych przez Norauto Polska 
          sp. z o.o. z siedzibą w Warszawie, ul. Jubilerska 10, jako administratora danych osobowych w rozumieniu rozporządzenia o ochronie danych osobowych z dnia 
          27 kwietnia 2016 (RODO)
        </p>

        <label class="form-label checkbox-wrapper" for="checkbox_1">
          <input name="checkbox_1" class="label-checkbox" type="checkbox" id="checkbox_1" required />
          <span class="checkmark"></span>
          <p class="checkbox-text">
            dla celów realizacji niniejszego procesu 
            rekrutacji*             
          </p>
        </label>

        <label class="form-label checkbox-wrapper" for="checkbox_2">
          <input name="checkbox_2" class="label-checkbox" type="checkbox" id="checkbox_2" />
          <span class="checkmark"></span>
          <p class="checkbox-text">
            dla celów realizacji przyszłych procesów 
            rekrutacyjnych, w okresie nie dłuższym 
            niż 12 miesięcy od dzisiejszej daty.            
          </p>
        </label>

        <p class="form-text is-required">
          * zaznaczenie klauzuli jest obowiązkowe
        </p>

        <div class="hidden-text-wrapper">
          <p class="hidden-text">
            Podanie danych osobowych jest dobrowolne i przyjmuję do wiadomości, że przysługuje mi prawo
            wglądu do treści moich danych, ich poprawiania lub uzupełniania oraz złożenia oświadczenia o
            cofnięciu niniejszej zgody w każdym momencie.
          </p>
          <p class="hidden-statute">
            Dodatkowo prosimy o zapoznanie się z następującą klauzulą informacyjną: <br /> <br />
            Zgodnie z art. 13 ust. 1 i ust. 2 ogólnego rozporządzenia o ochronie danych osobowych z dnia 27
            kwietnia 2016 r. (RODO) w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych
            osobowych i w sprawie swobodnego przepływu takich danych informujemy, że: <br /> <br />
            1) administratorem Pana/Pani danych osobowych jest Norauto Polska sp. z o.o. z siedzibą w
            Warszawie (04-190) przy ul. Jubilerskiej 10; <br /> <br />
            2) wszelkie pytania dotyczące Pana/Pani danych osobowych należy kierować na adres mailowy:
            ochronadanych@norauto.pl; <br /> <br />
            3) Pana/Pani dane osobowe przetwarzane będą w celu realizacji procesu rekrutacyjnego, na
            podstawie art. 6 ust 1 pkt a; <br /> <br />
            4) Pana/Pani dane osobowe nie będą przekazywane żadnym podmiotom zewnętrznym; <br /> <br />
            5) Pana/Pani dane osobowe nie będą przekazywane do państwa trzeciego/organizacji
            międzynarodowej; <br /> <br />
            6) Pana/Pani dane osobowe będą przechowywane przez okres trwania procesu rekrutacyjnego oraz
            w obowiązkowym okresie przechowywania dokumentacji związanej z rozpatrywaniem roszczeń o
            naprawienie szkody wyrządzonej czynem niedozwolonym na etapie rekrutacji, ustalonym zgodnie z
            odrębnymi przepisami; <br /> <br />
            7) posiada Pan/ Pani prawo dostępu do treści swoich danych oraz prawo ich sprostowania, usunięcia,
            ograniczenia przetwarzania, prawo do przenoszenia danych oraz prawo wniesienia sprzeciwu; <br /> <br />
            8) ma Pan/ Pani prawo wniesienia skargi do organu nadzorczego, w sytuacji gdy uzna Pan/ Pani, iż
            przetwarzanie Pana/ Pani danych osobowych narusza przepisy ogólnego rozporządzenia o ochronie
            danych osobowych z dnia 27 kwietnia 2016 r. (RODO); <br /> <br />
            9) podanie przez Pana/ Panią danych osobowych jest warunkiem wzięcia udziału w procesie
            rekrutacyjnym. Brak Pana/ Pani zgody na podanie danych osobowych oznacza niemożność realizacji
            procesu rekrutacyjnego.
          </p>
        </div>

        <button class="expand-btn" type="button">Rozwiń</button>
      </div>
      <div class="submit-wrapper">
        <input type="hidden" name="norauto_submit" value="1">
        <button class="submit-btn" type="submit">
          <span>Wyślij</span>
          <svg class="submit-btn-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
            <path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"/>
          </svg>
        </button>
      </div>
    </form>
  </div>
</div>
