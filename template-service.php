<?php /* Template Name: Job Offers Service Template */ get_header(); ?>
<div class="content-container">
  <div class="shop-header">
    <h2 class="shop-heading">
      <span class="shop-heading-part bold">
        Sklep i serwis
      </span>
      <span class="shop-heading-part normal">
        Oferty pracy
      </span>
    </h2>
  </div>

	<div class="job-offers-search-wrapper">
		<?php get_template_part( 'searchform' ); ?>
	</div>

	<div class="shop-content">
		<ul class="job-offers-container">
			<?php
				$args = array(
					'post_type' => array( 'oferta-pracy' ),
					'post_status' => array( 'publish' ),
					'category_name' => 'serwis',
					'hierarchical' => 1
				);
				$query = new WP_Query( $args );

				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
					$query->the_post();
					$miejscepracy = get_post_meta($post->ID, 'wpcf-miejsce-pracy', true);
					$deepest_category = wp_get_object_terms( $post->ID, 'category', array('orderby' => 'term_order', 'order' => 'ASC', 'fields' => 'all'));
			?>
			<li class="job-offer-item">
				<a href="<?php the_permalink(); ?>" class="job-offer-details">
					<h3 class="job-offer-title"><?php the_title(); ?></h3>
					<div class="job-offer-meta">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pin.svg" class="job-offer-icon" alt="Lokalizacja" />
						<p class="job-offer-location">
							<span class="job-offer-address">
								<?php echo $miejscepracy; ?>
							</span>
							<span class="job-offer-town">
								<?php echo $deepest_category[2]->name; ?>
							</span>
						</p>
					</div>
					<button class="job-offer-btn">
						Zobacz ofertę
					</button>
				</a>
			</li>
			<?php
					}
				}
				wp_reset_postdata();
			?>
		</ul>
	</div>

  <div class="shop-banner">
    <div class="banner-content">
      <p class="banner-heading">
        <span>
          NIE ZNALAZŁEŚ
        </span>
        <span>
          PASUJĄCEJ OFERTY?
        </span>
      </p>
      <p class="banner-subhead">
        Jeśli nie znalazłeś oferty, która spełnia Twoje oczekiwania, prześlij nam swoją aplikację.
				<br /><br />
        Jeżeli znajdziemy stanowisko odpowiadające Twojemu doświadczeniu i oczekiwaniom - skontaktujemy się z Tobą.
      </p>
    </div>
  </div>
</div>
<?php get_footer(); ?>
