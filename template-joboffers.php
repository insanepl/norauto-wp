<?php /* Template Name: Job Offers Template */ get_header(); ?>
<div class="content-container">
  <div class="job-offers-hub">
    <div class="container">
      <h1 class="hub-header">
        Oferty pracy
      </h1>
      <h3 class="hub-subheader">
        <span class="subheader-item">
          Serwis
        </span>
        <span class="subheader-item">
          Sklep
        </span>
        <span class="subheader-item">
          Centrala
        </span>
      </h3>

      <div class="hub-columns">
        <div class="hub-column">
          <div class="hub-image">
            <img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/box-serwis.jpg"
              alt="Serwis"
            />
            <h4 class="hub-title">
              Sklep i Serwis
            </h4>
          </div>
          <div class="box-content is-split">
            <div class="box-content-column">
              <p class="box-content-head">
                Serwis
              </p>
              <p class="box-content-paragraph">
                Samochody to twoja pasja?<br/>
								Praca w serwisie to podstawowy element naszej
                działalności. Rozwiązujemy tam najczęstsze problemy naszych klientów.
								<br />
								Dla nas Mechanik Samochodowy to nie tylko specjalista z zakresu mechaniki,
								ale osoba, która sprawia, że zadowoleni klienci chcą do nas wracać.
              </p>
            </div>
            <div class="box-content-column">
              <p class="box-content-head">
                Sklep
              </p>
              <p class="box-content-paragraph">
                Masz doświadczenie? A może dopiero szukasz swojej pierwszej pracy?
								<br />
								Pracując w sklepie nie jesteś tylko sprzedawcą, ale
                przede wszystkim doradcą, który zadba o najlepszą obsługę klientów.
              </p>
            </div>
          </div>
          <a class="hub-btn" href="<?php echo get_page_link( get_page_by_path( 'oferty-pracy-serwis' ) ); ?>">
            <span>
              Zobacz oferty pracy
            </span>
            <svg class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
              <path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"/>
            </svg>
          </a>
        </div>

        <div class="hub-column">
          <div class="hub-image">
            <img
            	src="<?php echo get_template_directory_uri(); ?>/assets/img/box-centrala.jpg"
            	alt="Centrala"
            />
            <h4 class="hub-title">
              Centrala i Stanowiska kierownicze
            </h4>
          </div>
          <div class="box-content is-split">
            <div class="box-content-column">
              <p class="box-content-head">
                Centrala
              </p>
              <p class="box-content-paragraph">
								Lubisz pracę zespołową, ale nie lubisz korporacyjnych struktur?
								<br />
								Nasza Centrala będzie dla Ciebie odpowiednim miejscem, gdzie będziesz mógł się
								wykazać w projektach lokalnych i międzynarodowych, mając realny wpływ na kształt
								i końcowy efekt.
              </p>
            </div>
            <div class="box-content-column">
              <p class="box-content-head">
                Stanowiska kierownicze
              </p>
              <p class="box-content-paragraph">
								Koordynowałeś już pracę zespołu? Masz duży plus na starcie!<br /><br />
								Wierzymy, że sprawnie działająca firma musi mieć sprawnie działających szefów.<br />
								Sprawdź, może poszukujemy Ciebie?
							</p>
            </div>
          </div>
          <a class="hub-btn" href="<?php echo get_page_link( get_page_by_path( 'oferty-pracy-centrala' ) ); ?>">
            <span>
              Zobacz oferty pracy
            </span>
            <svg class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
              <path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"/>
            </svg>
          </a>
        </div>
      </div>

      <div class="platforms-wrapper">
        <p class="platforms-heading">
          <strong>
            Szukaj naszych ofert
          </strong>
          na portalach
        </p>
        <div class="platforms-links">
          <a class="platform-link" href="https://www.linkedin.com/jobs/search/?f_C=18102337&location=Polska&locationId=OTHERS.worldwide" target="_blank" rel="noopener noreferrer">
            <svg class="platform-image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 456 446">
              <path d="M440 91.5v264.3c0 21.8-7.8 40.5-23.3 56s-34.2 23.3-56 23.3H96.4c-21.8 0-40.5-7.8-56-23.3s-23.3-34.2-23.3-56V91.5c0-21.8 7.8-40.5 23.3-56s34.2-23.3 56-23.3h264.3c21.8 0 40.5 7.8 56 23.3 15.6 15.4 23.3 34.1 23.3 56zm-289.9 24.7c-.2-9.5-3.5-17.4-9.9-23.7-6.4-6.2-15-9.4-25.6-9.4-10.6 0-19.3 3.1-26 9.4-6.7 6.2-10 14.1-10 23.7 0 9.4 3.3 17.2 9.8 23.5 6.5 6.3 15 9.5 25.5 9.5h.3c10.8 0 19.5-3.2 26.2-9.5 6.4-6.3 9.7-14.1 9.7-23.5zm-67.7 250H146v-191H82.4v191zm228.8 0h63.6V256.6c0-28.3-6.7-49.6-20.1-64.1-13.4-14.5-31.1-21.8-53.1-21.8-25 0-44.1 10.7-57.5 32.2h.6v-27.8H181c.6 12.1.6 75.8 0 191.1h63.6V259.4c0-7 .6-12.1 1.9-15.4 2.8-6.4 6.9-11.9 12.4-16.4 5.5-4.5 12.3-6.7 20.4-6.7 21.3 0 31.9 14.4 31.9 43.2v102.1z"/>
            </svg>
            <span class="platform-text">
              Ofert pracy na <strong>linkedin.com</strong>
            </span>
          </a>
          <a class="platform-link" href="https://www.pracuj.pl/praca/Norauto%20Polska%20Sp.%20z%20o.o.;kw" target="_blank" rel="noopener noreferrer">
            <svg class="platform-image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 456 446">
              <path d="M143.4 317c8.4 6 15.5 12.1 23.4 16.7 13.4 7.9 28.3 11.7 43.7 14.3 17 2.9 33.9 2.4 50.7-.7 41.5-7.5 72.8-28.7 89.5-68.8 11.1-26.7 12.6-54.2 7.3-82.2-6.4-33.6-23.4-60.1-52.8-78.2-11.6-7.1-24-11.8-37.3-14.7-17.9-4-35.9-5.1-54-3.3-18.2 1.8-35.9 5.7-51.3 16-6.5 4.4-12.3 10-19.1 15.6V118c0-16.5 1-13.9-14.3-14.1-8.5-.1-17-.1-25.5-.1-9.8 0-9.9.1-9.9 9.6V391c-6.4-5.7-11.9-10.1-16.9-15-21.2-20.5-37.9-44.1-48.6-71.7-3.8-10-6.5-20.4-9.7-30.6-5.7-18-6-36.6-5.5-55.2.3-12.2.7-24.7 3.4-36.6C24.4 147 39.5 115.4 63 88c19.9-23.1 43.6-41 70.8-54.3 15.8-7.7 32.6-12.9 49.9-16.6 19.7-4.2 39.6-5.6 59.5-4.3 16.6 1 32.9 4.1 48.9 8.9 22.9 6.8 44 17 63.4 30.8 33.6 23.9 58.5 54.9 74.3 93.2 13.7 33.1 17.9 67.5 14 102.8-4.9 44.3-22.7 83.2-52.4 116.3-22.9 25.5-50.5 44.5-82.6 57-32.6 12.8-66.3 16.9-100.9 14-20.9-1.7-41.2-6.6-60.8-14.6-3.8-1.6-3.6-4.3-3.6-7.3v-88c-.1-2.8-.1-5.7-.1-8.9z"/>
              <path d="M309.9 222.4c.3 22.4-4 42.1-15.9 59.7-9.5 14.1-22.9 22.8-39.3 27.1-19.1 5-38.4 4.6-57.2-.8-21.9-6.3-37.8-20.1-47.2-41.3-9-20.3-10.6-41.7-6.8-63 4.7-26.5 17.3-48.7 43.5-59.9 8.1-3.5 17-5.5 25.8-6.8 18.1-2.6 35.8-.7 52.5 7.2 19.1 9 31.6 23.8 38.9 43.4 4.4 11.7 6.2 23.6 5.7 34.4z"/>
            </svg>
            <span class="platform-text">
              Ofert pracy na <strong>pracuj.pl</strong>
            </span>
          </a>
          <a class="platform-link" href="https://www.goldenline.pl/firma/norauto/" target="_blank" rel="noopener noreferrer">
            <svg class="platform-image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 456 446">
              <path d="M212.3 199.6L126.4 46.7s-2-4.3-5.3-4.8c-5.4-1-6.6 4.2-6.6 4.2l-26.6 92.5c8.9 8.1 14.5 19.7 14.5 32.6 0 21.7-15.7 39.7-36.4 43.3l-25.9 89.9s-1.9 5.3 2.3 8.2c4.3 2.8 9.7-1.5 9.7-1.5L204.2 228s8.2-4.5 9.9-12c2.1-8.9-1.8-16.4-1.8-16.4z"/>
              <path d="M97.1 171.2c0 21.5-17.4 38.9-38.9 38.9s-38.9-17.4-38.9-38.9 17.4-38.9 38.9-38.9c21.5-.1 38.9 17.4 38.9 38.9zM394.8 334.3L248.1 242s-8-5-15.3-2.8c-8.9 2.6-13.5 9.7-13.5 9.7l-91.5 149.6s-2.7 3.8-1.6 7c1.8 5.2 6.9 3.8 6.9 3.8l91.3-21.3c1.6-22.9 20.6-41 43.9-41 16.2 0 30.4 8.8 38 21.9l88.6-20.7s5.6-.9 6-6c.3-5.4-6.1-7.9-6.1-7.9z"/>
              <path d="M307.1 390.8c0 21.5-17.4 38.9-38.9 38.9s-38.9-17.4-38.9-38.9 17.4-38.9 38.9-38.9 38.9 17.5 38.9 38.9z"/>
              <path d="M439.3 214.1l-66.9-73.4c-5.2 2.2-10.9 3.4-16.9 3.4-24.3 0-44.1-19.7-44.1-44.1 0-7.5 1.9-14.5 5.1-20.6l-58.3-64s-3.5-4.4-8.2-2.3c-4.6 2.1-3.8 9-3.8 9l-9 173.1s-.5 9.4 5 14.7c6.6 6.5 15 7.1 15 7.1l175.3 6.9s4.7.5 6.9-2c3.6-4.1-.1-7.8-.1-7.8z"/>
              <path d="M394.3 99.9c0 21.5-17.4 38.9-38.9 38.9s-38.9-17.4-38.9-38.9S333.9 61 355.4 61s38.9 17.5 38.9 38.9z"/>
            </svg>
            <span class="platform-text">
              Ofert pracy na <strong>goldenline.pl</strong>
            </span>
          </a>
          <a class="platform-link" href="https://norauto.olx.pl/" target="_blank" rel="noopener noreferrer">
            <svg class="platform-image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 456 446">
              <path d="M229.8 12.5c-117.2 0-212.1 95-212.1 212.1 0 117.2 95 212.1 212.1 212.1 117.2 0 212.1-95 212.1-212.1C442 107.4 347 12.5 229.8 12.5zm-78.5 275.8c-13.2 14.1-29.5 21.5-49.1 20.6-18.4-.9-33.4-9.3-45.2-23.2-27.2-31.8-27.8-82.1-1.6-114.7C66.6 157.1 81 148.2 99 146.2c16.1-1.8 30.6 2.7 43.4 12.4 16.2 12.4 25.6 29.1 29.8 48.8 1.4 6.5 1.8 13.3 2.3 16.9-.3 26.2-7.1 46.9-23.2 64zm76-28.7c-8.5.1-17.1.5-25.6.3-9.5-.2-17.8-6.5-17.7-17.5.1-12-.3-24-.5-36-.1-8.5-.3-17.1-.5-25.6-.2-9.4-.3-18.7-.5-28.1-.1-6.1-.4-12.1-.4-18.2 0-6.3 4.4-10.7 10.7-10.8 5.7-.1 11.3-.1 17 0 4.8.1 8.4 2.8 9.3 7.5.7 3.3.8 6.7.9 10 .1 4.8 0 9.7 0 14.5.2 0-.2 0 0 0 0 18.6.5 37.1.5 55.7 0 6.2 3.4 10.4 9.6 10.7 9.3.5 18.5.4 27.8.6 2 0 4.1.2 6.1.5 4.4.6 7.1 3 7.1 7.4.1 7-.1 14.1-1 21.1-.7 5.9-3.3 7.5-9.2 7.6-11.1.3-22.3.2-33.6.3zm173.6 51.6c-8.4 2.4-16.9 4.8-25.4 7-7.8 2-15.8-2-19.6-9.1-9-16.9-18.3-33.7-27.4-50.5-.3-.5-.7-.9-1.3-1.7-1.8 2.6-3.3 4.8-4.8 7.1-9.8 14.4-19.6 28.8-29.3 43.3-.4.5-.7 1.1-1.2 1.6-4.2 5.2-10.8 5.2-14.6-.3-4.1-6-8-12.1-11.6-18.4-4.1-7.2-2.9-16.7 2.8-23.2 12.8-14.6 25.6-29.1 38.5-43.6 1.8-2 1.9-3.5.8-5.8-9-17.9-17.8-35.9-26.7-53.9-4.7-9.4 1.4-19.5 11.8-19.8 7.8-.2 14.1 2.7 18.5 9.2 7.1 10.7 14.2 21.4 21.3 32 .9 1.3 1.8 2.5 2.9 4.2 2.7-3 5.2-5.8 7.6-8.6 9.4-10.9 18.8-21.8 28.3-32.5 4.5-5.1 10.3-7.8 17.3-7.2 4.7.4 7.7 3.5 7.8 8.2.1 3.4-1 6.4-2.9 9.2l-38.1 56.1c-1.3 1.9-1.3 3.3 0 5.2 16.6 24.8 33.1 49.6 49.6 74.5 1.2 1.8 2.3 3.9 2.6 6 1.3 5.2-1.7 9.6-6.9 11z"/>
              <path d="M120.2 177.9c-6.9-6.4-15.8-7.9-23.7-4-5.3 2.6-9.2 6.8-12.4 11.7-8.2 12.7-10.7 26.8-11.1 45 .5 3.5 1 10.3 2.4 16.9 2 10.3 6 19.8 13.4 27.6 10.1 10.6 22.7 10.8 32.9.3 2.9-3 5.6-6.6 7.5-10.3 9.7-18.9 10.7-38.9 6-59.3-2.5-10.6-6.8-20.4-15-27.9z"/>
            </svg>
            <span class="platform-text">
              Ofert pracy na <strong>olx.pl</strong>
            </span>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="benefits-content">
    <div class="container benefits-container">
      <h4 class="benefit-header">
        Benefity
      </h4>
      <div class="benefits-grid-wrapper">
        <div class="benefits-intro-text">
          <p class="intro-head">
            W Norauto dbamy o <strong>zadowolenie naszych pracowników.</strong>
          </p>
          <p class="intro-subhead">
            Prócz wynagrodzenia zyskujesz:
          </p>
        </div>
        <ul class="benefits-icons">
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/uop_c2.svg" />
            <p class="benefit-text">
              Umowa o pracę i stabilizacja
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/pewnosc_c2.svg" />
            <p class="benefit-text">
              Pewność zatrudnienia
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/atmosfera_c2.svg" />
            <p class="benefit-text">
              Przyjazna atmosfera pracy
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/przelozony_c2.svg" />
            <p class="benefit-text">
              Przełożony, który jest blisko 
              swoich pracowników
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/opieka_c2.svg" />
            <p class="benefit-text">
              Prywatna opieka medyczna
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/ubezpieczenie_c2.svg" />
            <p class="benefit-text">
              Ubezpieczenie na życie
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/sport_c2.svg" />
            <p class="benefit-text">
              Abonament sportowy
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/swiadczenia_c2.svg" />
            <p class="benefit-text">
              Świadczenia z Funduszu Socjalnego
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/rozwoj_c2.svg" />
            <p class="benefit-text">
              Możliwość rozwoju zawodowego
            </p>
          </li>
          <li class="benefit-item">
            <img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/szkolenia_c2.svg" />
            <p class="benefit-text">
              Szeroki wachlarz szkoleń
            </p>
          </li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/projekty_c2.svg" />
						<p class="benefit-text">
							Udział w ciekawych projektach
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/blisko_domu_c2.svg" />
						<p class="benefit-text">
							Gęsta sieć oddziałów
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/spotkania_integracyjne_c2.svg" />
						<p class="benefit-text">
							Spotkania integracyjne
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/bonus_polecenie_c2.svg" />
						<p class="benefit-text">
							Bonus za polecenie pracownika
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/zakres_samodzielnosci_c2.svg" />
						<p class="benefit-text">
							Duży zakres samodzielności
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/nauka_jezykow_c2.svg" />
						<p class="benefit-text">
							Nauka języków obcych
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/premie_c2.svg" />
						<p class="benefit-text">
							Premie za realizację celów, na które masz wpływ
						</p>
					</li>
					<li class="benefit-item">
						<img class="benefit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/benefits/wdrozenie_c2.svg" />
						<p class="benefit-text">
							Wdrożenie i przygotowanie do pracy
						</p>
					</li>
        </ul>
				<div class="benefits-hub-controls">
					<button class="benefits-hub-control jobHubPrev">
						<svg class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
							<path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"></path>
						</svg>
					</button>
					<button class="benefits-hub-control jobHubNext">
						<svg class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
							<path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"></path>
						</svg>
					</button>
				</div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
