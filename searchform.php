<?php $cat_id = get_cat_ID ('serwis'); ?>
<?php $fraza = $_GET['s']; $cat = $_GET['cat']; $position = $_GET['position']; ?>

<form class="job-offers-search-form search" method="get" action="<?php echo esc_url( home_url() ); ?>">
	<div class="job-offers-keyword">
		<p class="keyword-label">
			<strong>Wyszukaj</strong> ofertę pracy
		</p>

		<?php // Get the keyword ?>
		<input
			class="search-input"
			type="text"
			name="s"
			aria-label="Wyszukaj:"
			placeholder="Wpisz słowo kluczowe"
		>

		<input type="hidden" name="post_type" value="oferta-pracy" />
	</div>

	<div class="job-offers-selects">
		<?php // Get the Districts ?>
		<div class="job-offer-select">
			<p class="job-offer-select-label">
				WOJEWÓDZTWO
			</p>

			<?php
				$argsW = array(
  				'hide_empty' => 0,
					'parent' => $cat_id
				);

				$categories = get_categories($argsW);

				$selectW = '<select name="cat[]" id="state" class="job-offer-select-input"><option value="0" style="display: none;">Wybierz województwo</option><option value="'.$cat_id.'">Wszystkie województwa</option>';
					foreach($categories as $category) {
						$selectW .= '<option value="'.get_cat_ID ($category->slug).'">'.$category->cat_name.'</option>';
						$children = get_categories( array('parent' => $category->term_id) );
					}
				$selectW .= '</select>';

				echo $selectW;
			?>
		</div>

		<?php // Get the Towns ?>
		<div class="job-offer-select">
			<p class="job-offer-select-label">
				MIASTO
			</p>

			<select name="cat[]" id="town" class="job-offer-select-input">
				<option value="0" style="display: none;">Wybierz Miasto</option>
					<?php
						$args = array(
							'orderby' => 'name',
							'parent' => $cat_id,
							'hide_empty' => 0
						);

						$categories = get_categories( $args ); $first = true;

						foreach ($categories as $category) {
							$category_id = $category->term_id;
							$children = $wpdb->get_results( "SELECT term_id FROM $wpdb->term_taxonomy WHERE parent=$category_id" );
							$no_children = count($children);

							if ($no_children > 0) {
								$args2 = array(
									'orderby' => 'name',
									'parent' => 2,
									'hide_empty' => 0
								);

								$args2["parent"]=$category->term_id;
								$subcategories = get_categories( $args2 );

								foreach ($subcategories as $subcategory) {
									echo '<option value='.$subcategory->term_id.' data-pub='.$subcategory->parent.'>'.$subcategory->cat_name.'</option>';
								}
							}
						}
					?>
			</select>
		</div>

		<?php // Get Positions ?>
		<div class="job-offer-select">
			<p class="job-offer-select-label">
				STANOWISKO
			</p>

			<?php
				$key_name = get_post_custom_values($key = 'wpcf-poziom-stanowiska');
				echo $key_name[0];
			?>

			<select name="position" class="job-offer-select-input">
				<option value="" style="display: none;">Wybierz Stanowisko</option>
				<option value="Stażysta / Praktykant">Stażysta / Praktykant</option>
				<option value="Pomocnik">Pomocnik</option>
				<option value="Pracownik fizyczny">Pracownik fizyczny</option>
				<option value="Asystent">Asystent</option>
				<option value="Specjalista">Specjalista</option>
				<option value="Kierownik">Kierownik</option>
				<option value="Dyrektor">Dyrektor</option>
			</select>
		</div>
	</div>

	<button class="job-offer-search-btn search-submit" type="submit">
		<span>
			Szukaj
		</span>
		<svg class="job-btn-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.8 67.9">
			<path d="M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z"/>
		</svg>
	</button>
</form>
