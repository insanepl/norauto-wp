<?php
require get_template_directory() . '/inc/walker.php';

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu_class'      => 'menu_class',
		'menu_id'         => 'menu_id',
		'echo'            => true,
		'container'		  => false,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="navbar">%3$s</ul>',
		'depth'           => 0,
		'walker' 		  => new Walker_Norauto_Menu()
		)
	);
}

function menu_set_dropdown( $sorted_menu_items, $args ) {
    $last_top = 0;
    foreach ( $sorted_menu_items as $key => $obj ) {
        if ( 0 == $obj->menu_item_parent ) {
            $last_top = $key;
        } else {
            $sorted_menu_items[$last_top]->classes['has-submenu'] = 'has-submenu';
        }
    }
    return $sorted_menu_items;
}

add_filter( 'wp_nav_menu_objects', 'menu_set_dropdown', 10, 2 );

function html5blank_header_scripts() {}

function html5blank_conditional_scripts()
{
    if (is_page('CUSTOMPAGESCRIPT')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname');
    }
}

function html5blank_styles()
{
    wp_register_style('norauto', get_template_directory_uri() . '/style.css?v=' . rand(1,1000), array(), '1.0', 'all');
    wp_enqueue_style('norauto');
}

function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

if (function_exists('register_sidebar'))
{
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

function process_form()
{
    // if this is the admin then bail early
    if (is_admin()) {
        return;
    }

    // some standard values we are going to use later
    global $post, $norautoMessages;

    // check whether the form has already been posted
    if (isset($_POST['norauto_submit'])) {
        $norautoMessages = [];

        // array of allowed mime types, might be set as custom fields in the post, then we want to apply_filter with a method that will get allowed files from the post
        $allowedMimeTypes = [
            'pdf'		=> 'application/pdf',
            'word'		=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'word_old'	=> 'application/msword',
            'pages'		=> 'application/vnd.apple.pages',
            'open_text'	=> 'application/vnd.oasis.opendocument.text',
            'rich_text'	=> 'application/rtf',
            'text'		=> 'text/plain',
            'jpe'       => 'image/jpeg',
            'pjpe'      => 'image/pjpeg',
            'jpeg'      => 'image/jpeg',
            'pjpeg'     => 'image/pjpeg',
            'jpg'       => 'image/jpeg',
            'pjpg'      => 'image/pjpeg',
            'png'       => 'image/png'
        ];
        $wpUploadDir      = wp_upload_dir();

        // check that the wp_handle_upload function is loaded
        if (!function_exists('wp_handle_upload'))
            require_once(ABSPATH . 'wp-admin/includes/file.php');

        // get the uploaded cv file information
        $uploadedCVFile = $_FILES['add_cv'];
        // check that the $_FILES var is an array
        if (!is_array($uploadedCVFile) || $uploadedCVFile['size'] == 0) {
            // add an error message
            $norautoMessages['attachment_failed'] = [
                'type'    => 'error',
                'message' => 'Error: File attachment failed.'
            ];
            // go no further as file missing
            return;
        }
        // sanitize the uploaded file name
        $uploadedCVFileName = sanitize_text_field($uploadedCVFile['name']);
        // check we have a file to upload
        if ($uploadedCVFileName != '') {
            // upload the file to wp uploads dir
            $movedFile = wp_handle_upload($uploadedCVFile, ['test_form' => false]);
            // get file type of the uploaded file
            $filetype = wp_check_filetype($movedFile['url'], null);
            // check uploaded file is in allowed mime types array
            if (!in_array($filetype['type'], $allowedMimeTypes)) {
                // add an error message
                $norautoMessages['cv_type_failed'] = [
                    'type'    => 'error',
                    'message' => 'Error: CV is not an allowed file type.'
                ];
                // go no further as file type not allowed, remove file
                unlink($wpUploadDir['path'] . '/' . basename($movedFile['file']));
                return;
            }
        }
        // get the other docs file information
        $uploadedOtherFile     = $_FILES['other_docs'];
        $uploadedOtherFileName = (is_array($uploadedOtherFile) && $uploadedOtherFile['size'] > 0) ? sanitize_text_field($uploadedOtherFile['name']) : false;
        // check we have a file to upload
        if ($uploadedOtherFileName != '') {
            // upload the file to wp uploads dir
            $movedOtherFile = wp_handle_upload($uploadedOtherFile, ['test_form' => false]);
            // get file type of the uploaded file
            $filetype = wp_check_filetype($movedOtherFile['url'], null);
            // check uploaded file is in allowed mime types array
            if (!in_array($filetype['type'], $allowedMimeTypes)) {
                // add an error message
                $norautoMessages['other_doc_type_failed'] = [
                    'type'    => 'error',
                    'message' => 'Error: Other document uploaded is not an allowed file type.'
                ];
                // set $uploadedOtherFileName to 'error' so it won't be uploaded, remove file
                $uploadedOtherFileError = true;
                unlink($wpUploadDir['path'] . '/' . basename($movedOtherFile['file']));
            }
        }

        // filter incoming data @todo: check for required fields here... + in case of error - add msg to $norautoMessages and return, use global in the job post template
        $applicantName   = sanitize_text_field($_POST['name']);
        $applicantEmail  = sanitize_email($_POST['email']);
        $applicantPhone  = sanitize_text_field($_POST['phone_number']);
        $applicantMessage = wp_kses_post($_POST['employer_message']);
        // position
        $applicantPosition = sanitize_text_field($_POST['position_select']);
        // workplace
        $applicantWorkplace  = empty($_POST['workplace_select_county']) ? '' : sanitize_text_field($_POST['workplace_select_county']);
        $applicantWorkplace2 = empty($_POST['workplace_select_town']) ? '' : sanitize_text_field($_POST['workplace_select_town']);
        // checkboxes
        $applicantCheckbox1 = (!empty($_POST['checkbox_1']) && $_POST['checkbox_1'] == 'on') ? 'Tak' : 'Nie';
        $applicantCheckbox2 = (!empty($_POST['checkbox_2']) && $_POST['checkbox_2'] == 'on') ? 'Tak' : 'Nie';

        // other useful info we want to send
        $jobRef   = get_post_meta($post->ID, 'wpcf-nr-ref', true);
        $jobTitle = $post->post_title;
        $jobUrl   = get_permalink($post);

        // email variables
        $emailSubject   = 'New Job Application Submitted - ' . esc_html($jobTitle);
        $emailHeaders[] = 'Content-Type: text/html; charset=UTF-8' . "\r\n";
        $emailHeaders[] = 'From: ' . esc_html($applicantName) . ' <' . $applicantEmail . '>';
        // @todo: IMO cool if we could assign "agent" to the job post, then we pull email from the post, for now we will use admin's email
        $recipients   = ['praca@norauto.pl'];
        $recipients[] = get_option('admin_email');
        /* get the contact email
        $contactEmail = get_post_meta($post->ID, 'wpcf_MOCKEMAIL', true);// change MOCKEMAIL to whatever!!!!
        if ($contactEmail != '') {
            $recipients[] = sanitize_email($contactEmail);
        }
        */
        $recipients = array_unique($recipients);// just in case admin email is set to "insanepl@gmail.com" ;)

        // build the content of the email and set other email values
        $emailContent = '
            <p>Nowa aplikacja na stanowisko numer ref. ' . $jobRef . ':</p>
			<ul>
				<li>Imię: ' . $applicantName . '</li>
				<li>Adres email: ' . $applicantEmail . '</li>
				<li>Numer telefonu: ' . $applicantPhone . '</li>
				<li>Aplikował na: ' . esc_html($jobTitle) . ' <a href="' . esc_url($jobUrl) . '">(' . esc_url($jobUrl) . ')</a></li>
				<li>Pozycja: ' . $applicantPosition . '</li>
				<li>Województwo: ' . $applicantWorkplace . '</li>
				<li>Miasto: ' . $applicantWorkplace2 . '</li>
			</ul>
			<br />' . wpautop($applicantMessage) . '<br /><br />
			<p>Wyraził zgody:</p>
			<ul>
				<li>dla celów realizacji niniejszego procesu rekrutacji: ' . $applicantCheckbox1 . '</li>
				<li>dla celów realizacji przyszłych procesów rekrutacyjnych, w okresie nie dłuższym  niż 12 miesięcy od dzisiejszej daty: ' . $applicantCheckbox2 . '</li>
			</ul>
		';

        // set attachments - the cv
        $emailAttachments = [$wpUploadDir['path'] . '/' . basename($movedFile['file'])];
        // other doc if was uploaded and mime was ok
        if ($uploadedOtherFileName != '' && !isset($uploadedOtherFileError)) {
            $emailAttachments[] = $wpUploadDir['path'] . '/' . basename($movedOtherFile['file']);
        }

        // send the mail
        add_filter('wp_mail_content_type', 'set_html_content_type');// send as html
        $emailSent = wp_mail(
            $recipients,
            $emailSubject,
            $emailContent,
            $emailHeaders,
            $emailAttachments
        );
        remove_filter('wp_mail_content_type', 'set_html_content_type');// turn off sending as html

        // @todo: remove uploaded file?
        /**
         * Concept below (great plugin: WP Broadbean - works like a charm, learnt much from it!)
         * - once applicant successfully applies for the job we could add the application as post
         * - wp can store CVs and users, job agents can download it (GDPR - "RODO" compatible because we hold the info to process it later, CV can be deleted once applicant wish to do it etc.)
         */
        /*
        // get the wp upload directory
        $wpUploadDir = wp_upload_dir();
        // setup the attachment data
        $attachment = array(
            'post_mime_type' => $filetype['type'],
            'post_title'     => preg_replace('/\.[^.]+$/', '', $uploadedCVFileName),
            'post_content'   => '',
            'guid'           => $wpUploadDir['url'] . '/' . basename($movedFile['file']),
            'post_status'    => 'inherit'
        );
        // insert the application post
        $applicationId = wp_insert_post(
            [
                'post_type'		=> 'norautop_application',// post type not published for guests?
                'post_title'	=> esc_html($applicantName),
                'post_status'	=> 'publish',
                'post_content'	=> $applicantMessage
            ]
        );

        // check the application post has been added
        if ($applicationId != 0) {
            // set the post meta data (custom fields)
            add_post_meta($applicationId, 'wpcf-nr-ref', $jobRef, true);
            add_post_meta($applicationId, 'wpcf-MOCKEMAIL', $applicantEmail, true);// Change MOCKEMAIL to whatever...!!!

            // check we have a file to attach
            if ($uploadedCVFileName != '') {
                // add the attachment from the uploaded file
                $applicationAttachmentId = wp_insert_attachment($attachment, $movedFile['file'], $applicationId);
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attachmentData = wp_generate_attachment_metadata($applicationAttachmentId, $movedFile['file']);
                wp_update_attachment_metadata($applicationAttachmentId, $attachmentData);

                // this could be a setting...
                if (false) {
                    wp_delete_attachment($applicationAttachmentId, true);
                }
            }

            // add the message to global var
            $norautoMessages['application_success'] = [
                'type'    => 'success',
                'message' => 'Thank you. Your application has been received.'
            ];
        }
        Concept end */

        // check if the email was sent ok
        $link = get_permalink(get_page_by_path('sent'));
        if ($emailSent) {
            $norautoMessages['application_success'] = [
                'type'    => 'success',
                'message' => 'Thank you. Your application has been received.'
            ];

            // remove the file
            unlink($wpUploadDir['path'] . '/' . basename($movedFile['file']));
            if ($uploadedOtherFileName != '' && !isset($uploadedOtherFileError)) {
                unlink($wpUploadDir['path'] . '/' . basename($movedOtherFile['file']));
            }

            // redirect with success
            wp_redirect($link . '?success=true');

            // so the redirect can finish its job
            die();
        } else {
            $norautoMessages['application_failed'] = [
                'type'    => 'error',
                'message' => 'There was an server error processing your request.'
            ];

            // redirect with error
            wp_redirect($link . '?success=false');

            // so the redirect can finish its job
            die();
        }
    }
}

function remove_admin_bar()
{
    return false;
}

/**
 * returns string for use when setting wp_mail content type
 */
function set_html_content_type() {
    return 'text/html';
}

function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Add Actions
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('wp', 'process_form', 10);// Process the apply-form, sanitizes and sends data submitted

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

function remove_json_api () {
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );
    add_filter( 'embed_oembed_discover', '__return_false' );
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );

}
add_action( 'after_setup_theme', 'remove_json_api' );
if ( !is_admin() ) wp_deregister_script('jquery');

?>
