<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
		<title>
			<?php wp_title(''); ?>
			<?php if(wp_title('', false)) { echo ' :'; } ?>
			<?php bloginfo('name'); ?>
		</title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="//ajax.googleapis.com" rel="dns-prefetch">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
		<script>
		  WebFont.load({
			google: {
			  families: ['Open Sans:400,600,700,800:latin-ext']
			}
		  });
		</script>
        <!-- <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon"> -->
        <!-- <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed"> -->
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<div class="site-container">
          <div class="site-nav">
            <div class="container">
              <div class="nav-items">
                <a class="logo-link" href="<?php echo home_url(); ?>">
                  <svg class="logotype" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 437.04 73.5">
                    <path fill="#fff" d="M347.9 25.2h23l2.2-14.9h-23L351.4 0h-10.1l-6.1 10.3-7.7 5-1.3 9.9h5l-3.7 25.9c-2.4 17.3 4.2 24.3 37.3 21L367 57c-20.8 2.6-23.5-1.1-22.4-9zM231.7 56.6a23.17 23.17 0 0 1-10.7 2.2c-11.2 0-16.2-6.4-14.7-17.5 1.5-10.7 9.2-16.7 19.7-16.7a26.13 26.13 0 0 1 9.9 1.8zm-42.9-15c-2.9 20.8 8.5 31.6 24.1 31.6 6.7 0 13.9-2.2 19.1-7.3l.2 6.2h14l7.9-57c-7.5-3.7-15.1-5.7-26.1-5.7-18.6 0-36.2 10.8-39.2 32.2M300.8 43.4c-1.5 11.6-7 15.8-14.7 15.8-8.5 0-12.3-4.2-10.7-15.8l4.6-33.1h-16.7l-4.8 34c-3.1 22.1 8.3 29.2 25.2 29.2 16.4 0 30.5-7 33.5-29.2l4.8-34h-16.7zM128.5 41.2c-1.5 10.7-9.4 16.9-18.2 16.9S95.4 52 96.9 41.2s9.4-17.1 18.2-17.1 14.9 6.4 13.4 17.1M117.3 8.8A37.5 37.5 0 0 0 80 41.2c-2.9 19.5 10.1 32.2 28.1 32.2a37.47 37.47 0 0 0 37.3-32.2c2.6-19.5-10.1-32.4-28.1-32.4M56.4 47.8L28.7 5.5c-2.2-3.3-3.9-3.7-9.6-3.7h-8.8L0 72.1h16.2l6.4-45.7 28 42c1.8 2.6 4.4 3.7 8.3 3.7H69L79.1 1.5H62.9zM191.8 10.3c-12.5-1-16.2 1.9-23.1 10.7l-.1-10.7h-12.9l-8.8 61.8h16.7l4.6-32.3c1.8-10.9 7.7-13.6 21.3-12.7z" class="cls-1"/>
                    <path fill="#f9b000" d="M419.8 41.2c-1.5 10.7-9.4 16.9-18.2 16.9s-14.9-6.1-13.4-16.9 9.4-17.1 18.2-17.1 14.9 6.4 13.4 17.1M408.6 8.8a37.5 37.5 0 0 0-37.3 32.4c-2.8 19.5 10.1 32.2 28.1 32.2a37.47 37.47 0 0 0 37.3-32.2c2.6-19.5-10.1-32.4-28.1-32.4"/>
                  </svg>
                </a>
                <button class="burger-menu">
                  <span></span>
                  <span></span>
                  <span></span>
                </button>
              </div>
              <?php html5blank_nav(); ?>
            </div>
          </div>
        </div>
