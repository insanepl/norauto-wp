const config = require( './wpgulp.config.js' );
const gulp = require( 'gulp' );

const sass = require( 'gulp-sass' );
const minifycss = require( 'gulp-uglifycss' );
const autoprefixer = require( 'gulp-autoprefixer' );
const mmq = require( 'gulp-merge-media-queries' );
const rtlcss = require( 'gulp-rtlcss' );


const concat = require( 'gulp-concat' );
const uglify = require( 'gulp-uglify' );
const babel = require( 'gulp-babel' );

const imagemin = require( 'gulp-imagemin' );

const rename = require( 'gulp-rename' );
const lineec = require( 'gulp-line-ending-corrector' );
const filter = require( 'gulp-filter' );
const sourcemaps = require( 'gulp-sourcemaps' );
const notify = require( 'gulp-notify' );
const browserSync = require( 'browser-sync' ).create();
const wpPot = require( 'gulp-wp-pot' );
const sort = require( 'gulp-sort' );
const cache = require( 'gulp-cache' );
const remember = require( 'gulp-remember' );
const plumber = require( 'gulp-plumber' );
const beep = require( 'beepbeep' );

const errorHandler = r => {
	notify.onError( '\n\n❌  ===> ERROR: <%= error.message %>\n' )( r );
	beep();

	// this.emit('end');
};

const browsersync = done => {
	browserSync.init({
		proxy: config.projectURL,
		open: config.browserAutoOpen,
		injectChanges: config.injectChanges,
		watchEvents: [ 'change', 'add', 'unlink', 'addDir', 'unlinkDir' ]
	});
	done();
};

const reload = done => {
	browserSync.reload();
	done();
};

gulp.task( 'styles', () => {
	return gulp
		.src( config.styleSRC, { allowEmpty: true })
		.pipe( plumber( errorHandler ) )
		.pipe( sourcemaps.init() )
		.pipe(
			sass({
				errLogToConsole: config.errLogToConsole,
				outputStyle: config.outputStyle,
				precision: config.precision
			})
		)
		.on( 'error', sass.logError )
		.pipe( sourcemaps.write({ includeContent: false }) )
		.pipe( sourcemaps.init({ loadMaps: true }) )
		.pipe( autoprefixer( config.BROWSERS_LIST ) )
		.pipe( sourcemaps.write( './' ) )
		.pipe( lineec() )
		.pipe( gulp.dest( config.styleDestination ) )
		.pipe( filter( '**/*.css' ) )
		.pipe( mmq({ log: true }) )
		.pipe( browserSync.stream() )
		.pipe( rename({ suffix: '.min' }) )
		.pipe( minifycss({ maxLineLen: 10 }) )
		.pipe( lineec() )
		.pipe( gulp.dest( config.styleDestination ) )
		.pipe( filter( '**/*.css' ) )
		.pipe( browserSync.stream() )
		.pipe( notify({ message: '\n\n✅  ===> STYLES — completed!\n', onLast: true }) );
});

gulp.task( 'vendorsJS', () => {
	return gulp
		.src( config.jsVendorSRC, { since: gulp.lastRun( 'vendorsJS' ) })
		.pipe( plumber( errorHandler ) )
		.pipe(
			babel({
				presets: [
					[
						'@babel/preset-env',
						{
							targets: { browsers: config.BROWSERS_LIST }
						}
					]
				]
			})
		)
		.pipe( remember( config.jsVendorSRC ) )
		.pipe( concat( config.jsVendorFile + '.js' ) )
		.pipe( lineec() )
		.pipe( gulp.dest( config.jsVendorDestination ) )
		.pipe(
			rename({
				basename: config.jsVendorFile,
				suffix: '.min'
			})
		)
		.pipe( uglify() )
		.pipe( lineec() )
		.pipe( gulp.dest( config.jsVendorDestination ) )
		.pipe( notify({ message: '\n\n✅  ===> VENDOR JS — completed!\n', onLast: true }) );
});

gulp.task( 'customJS', () => {
	return gulp
		.src( config.jsCustomSRC, { since: gulp.lastRun( 'customJS' ) })
		.pipe( plumber( errorHandler ) )
		.pipe(
			babel({
				presets: [
					[
						'@babel/preset-env',
						{
							targets: { browsers: config.BROWSERS_LIST }
						}
					]
				]
			})
		)
		.pipe( remember( config.jsCustomSRC ) )
		.pipe( concat( config.jsCustomFile + '.js' ) )
		.pipe( lineec() )
		.pipe( gulp.dest( config.jsCustomDestination ) )
		.pipe(
			rename({
				basename: config.jsCustomFile,
				suffix: '.min'
			})
		)
		.pipe( uglify() )
		.pipe( lineec() )
		.pipe( gulp.dest( config.jsCustomDestination ) )
		.pipe( notify({ message: '\n\n✅  ===> CUSTOM JS — completed!\n', onLast: true }) );
});

gulp.task( 'images', () => {
	return gulp
		.src( config.imgSRC )
		.pipe(
			cache(
				imagemin([
					imagemin.gifsicle({ interlaced: true }),
					imagemin.jpegtran({ progressive: true }),
					imagemin.optipng({ optimizationLevel: 4 }), // 0-7 low-high.
					imagemin.svgo({
						plugins: [ { removeViewBox: true }, { cleanupIDs: false } ]
					})
				])
			)
		)
		.pipe( gulp.dest( config.imgDST ) )
		.pipe( notify({ message: '\n\n✅  ===> IMAGES — completed!\n', onLast: true }) );
});

gulp.task( 'clearCache', function( done ) {
	return cache.clearAll( done );
});

gulp.task(
	'default',
	gulp.parallel( 'styles', 'vendorsJS', 'customJS', 'images', browsersync, (done) => {
		gulp.watch( config.watchPhp, reload );
		gulp.watch( config.watchStyles, gulp.parallel( 'styles' ) );
		gulp.watch( config.watchJsVendor, gulp.series( 'vendorsJS', reload ) );
		gulp.watch( config.watchJsCustom, gulp.series( 'customJS', reload ) );
		gulp.watch( config.imgSRC, gulp.series( 'images', reload ) );
		done();
	})
);
