<?php /* Template Name: Homepage Template */ get_header(); ?>
<div class='content-container'>
	<div class='homepage-hero'>
		<div class='hero-container'>
			<div class='hero-image'>
				<h1 class='hero-header'>
					Kim jesteśmy
				</h1>
				<div class='hero-content'>
					<p class='hero-paragraph'>
						Norauto to połączenie sklepu motoryzacyjnego - oferującego większość części i akcesoriów do
						naprawy oraz utrzymania samochodu, a także serwisu samochodów osobowych, skupionego na
						naprawach szybkich oraz średniozaawansowanych.
					</p>
					<p class='hero-paragraph'>W naszych strukturach umożliwiamy
						Działamy globalnie. Zatrudniamy ponad 21 000 osób
						w 12 krajach na świecie. Norauto działa w ramach międzynarodowej grupy Mobivia, skupiającej
						19 różnych marek z zakresu szeroko pojętej mobilności.
					</p>
					<p class='hero-paragraph'>
						Obecnie w Polsce sieć liczy 39 sklepów i serwisów samochodowych zlokalizowanych w
						największych miastach Polski. W kolejnych latach planujemy dalszy rozwój firmy oraz
						wzmocnienie pozycji na rynku. W Norauto w Polsce pracuje 700 osób – poszczególne
						centra zatrudniają od 14 do 30 osób. W naszych strukturach umożliwiamy rozwój w
						sklepach, serwisach oraz centrali firmy w Warszawie.
					</p>
				</div>
			</div>
			<div class='hero-blue-bg'>
				<div class='hero-blue-content'>
					<h2 class='hero-blue-heading'>
						firma
						<span class='orange'>
					fajnych ludzi
				</span>
					</h2>
					<a class='hero-apply-btn' href="<?php echo get_page_link( get_page_by_path( 'oferty-pracy' ) ); ?>">
				<span>
				  Aplikuj
				</span>
						<svg
							xmlns='http://www.w3.org/2000/svg'
							viewBox='0 0 69.8 67.9'
						>
							<path
								d='M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z'/>
						</svg>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class='map-section'>
		<div class='container map-container'>
			<h3 class='map-section-heading'>
				Nasze centra
			</h3>
			<div class='map'>
				<iframe src="https://snazzymaps.com/embed/126502" width="100%" height="600px" style="border:none;"></iframe>
			</div>
			<div class='map-section-box'>
				<h3 class='map-section-heading in-box'>
					Nasze centra
				</h3>
				<h4 class='map-section-subheading'>
					<span>
						39 punktów
					</span>
					w największych miastach Polski
				</h4>
			</div>
		</div>
	</div>

	<div class='testimonials'>
		<div class='testimonial-container'>
			<h3 class='testimonial-header'>
				Dlaczego warto do nas dołączyć?
			</h3>
			<ul class='testimonial-slider'>
				<li class='testimonial-item'>
					<div class='testimonial-title-wrapper'>
						<p class='testimonial-title'>MOJA PRACA JEST MOJĄ PASJĄ</p>
					</div>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Centrala</p>
							<p class='testimonial-details-name'>Dariusz</p>
							<p class='testimonial-details-misc'>14 lat w Norauto</p>
						</div>
						<div class='testimonial-quote'>
              Dzięki Norauto mam możliwość robienia tego co lubię i mogę szczerze powiedzieć, że moja praca jest
              jednocześnie moją pasją. Co więcej, po kilkukrotnych awansach zajmuję stanowisko, które umożliwia
              mi współuczestnictwo w budowaniu firmy na polskim rynku, mam wpływ na kierunek jej rozwoju
              oraz ofertę skierowaną do klientów. Jestem z tego dumny. <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' 
									src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-8.jpg'
								 alt='Dariusz'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>Praca daje mi poczucie własnej wartości i wiele satysfakcji</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Centrala</p>
							<p class='testimonial-details-name'>Katarzyna</p>
							<p class='testimonial-details-misc'>5 lat w Norauto</p>
						</div>
						<div class='testimonial-quote'>
							Gdybym miała wskazać, czym Norauto wyróżnia się na tle konkurencji, 
							byłaby to stabilność zatrudnienia oraz wysokie zaufanie przełożonych 
							do współpracowników. Dzięki temu czuję się bezpiecznie w pracy, 
							a jednocześnie wiem, że moje pomysły i opinie mają znaczenie. <br/> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' 
								 src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-5.jpg'
								 alt='Katarzyna'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>Praca daje mi poczucie własnej wartości i wiele satysfakcji</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Centrala</p>
							<p class='testimonial-details-name'>Ewa</p>
							<p class='testimonial-details-misc'>3 lata i 9 miesięcy w Norauto</p>
						</div>
						<div class='testimonial-quote'>
							Pomimo tego, że mam dość krótki staż pracy, 
							Norauto stało się dla mnie niejako sposobem na życie. 
							Praca daje mi poczucie własnej wartości i wiele satysfakcji. 
							Natomiast ludzie, z którymi pracuję to wyjątkowe osoby, 
							które z pewnością mogę nazwać przyjaciółmi. <br/> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-6.jpg'
								 alt='Ewa'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>Praca daje mi poczucie własnej wartości i wiele satysfakcji</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Centrala</p>
							<p class='testimonial-details-name'>Bartek</p>
							<p class='testimonial-details-misc'>W Norauto od 10 lat</p>
						</div>
						<div class='testimonial-quote'>
							Pracując w Norauto mogę realizować się zawodowo, 
							korzystać ze swojego wykształcenia i rozwijać pasje. 
							Ogromnym atutem jest poczucie tego, że przyczyniam się do rozwoju firmy. 
							Mogę proponować innowacyjne rozwiązania, które mają dużą szansę być zrealizowane. 
							Daje mi to wielką satysfakcję. <br/> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-1.jpg'
								 alt='Bartek'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>Praca daje mi poczucie własnej wartości i wiele satysfakcji</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Sklep</p>
							<p class='testimonial-details-name'>Janusz</p>
							<p class='testimonial-details-misc'>11 lat w Norauto</p>
						</div>
						<div class='testimonial-quote'>
              Mój staż pracy mówi chyba sam za siebie. Norauto daje mi stabilność finansową i jednocześnie
              możliwość samorealizacji. W tej pracy uwielbiam to, że trudno tutaj o rutynę. Każdy dzień jest inny i
              stawia nowe wyzwania, bo nie ma dwóch takich samych klientów. <br/> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-2.jpg'
								 alt='Janusz'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>TUTAJ ROBIĘ TO CO LUBIĘ</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Sklep</p>
							<p class='testimonial-details-name'>Ewa</p>
							<p class='testimonial-details-misc'>20 lat w Norauto</p>
						</div>
						<div class='testimonial-quote'>
              Tutaj robię to co lubię, czyli mam codziennie kontakt z klientem. Norauto dało mi prawdziwą
              możliwość rozwoju: w ostatnich latach dwukrotnie awansowałam. Doceniam to, że mam fajnego,
              doświadczonego szefa, który dobrze organizuje pracę całego oddziału. Cieszę się, że w naszych
              sklepach stawiamy na profesjonalną obsługę klienta, przez co jesteśmy liderem na rynku
              motoryzacyjnym.
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-5.jpg'
									alt='Ewa'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>PRACA DAJE MI MOŻLIWOŚĆ ROZWOJU, UMOŻLIWIA CIĄGŁĄ NAUKĘ I STWARZA WYZWANIA</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Serwis</p>
							<p class='testimonial-details-name'>Tomek</p>
							<p class='testimonial-details-misc'>W Norauto od 14 lat</p>
						</div>
						<div class='testimonial-quote'>
              Praca w Norauto daje mi możliwość rozwoju, umożliwia ciągłą naukę i stwarza wyzwania, dzięki
              czemu, po wielu latach pracy, nie odczuwam w najmniejszym stopniu rutyny ani poczucia wypalenia.
              W firmie panuje bardzo przyjazna i serdeczna atmosfera, komunikujemy się ze sobą bezpośrednio,
              dzięki czemu sprawnie rozwiązujemy pojawiające się problemy. <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-3.jpg'
									alt='Tomek'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>WIDZĘ REZULTATY PODEJMOWANYCH PRZEZE MNIE DZIAŁAŃ</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Serwis</p>
							<p class='testimonial-details-name'>Łukasz</p>
							<p class='testimonial-details-misc'>10 lat w Norauto</p>
						</div>
						<div class='testimonial-quote'>
              Praca tutaj umożliwia mi ciągły rozwój zawodowy i daje satysfakcję, ponieważ nie jestem trybikiem
              olbrzymiej machiny, widzę rezultaty podejmowanych przeze mnie działań. Co doceniam? Przede
              wszystkim ludzi, z którymi pracuję na co dzień! <br /> <br /> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-4.jpg'
									alt='Łukasz'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>TO TAKA PRACA, DO KTÓREJ PRZYCHODZISZ Z PRZYJEMNOŚCIĄ</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Centrala</p>
							<p class='testimonial-details-name'>Przemek</p>
							<p class='testimonial-details-misc'>W Norauto 2 lata i 6 miesięcy</p>
						</div>
						<div class='testimonial-quote'>
              Pracuję tutaj z fantastycznymi ludźmi. Uczę się, poszerzam swoją wiedzę, zdobywam nowe
              doświadczenia. Przełożeni mi ufają i dają dużą swobodę w działaniu. Ogromnym plusem jest również
              brak barier we współpracy z Zarządem. To taka praca, do której przychodzisz z przyjemnością. <br/> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-8.jpg'
									alt='Przemek'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>CIĄGŁY ROZWÓJ, WSPÓŁPRACA MIĘDZY PRACOWNIKAMI ORAZ SATYSFAKCJA</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Sklep</p>
							<p class='testimonial-details-name'>Iza</p>
							<p class='testimonial-details-misc'>W Norauto 17 lat</p>
						</div>
						<div class='testimonial-quote'>
              Gdybym miała w kilku słowach streścić swoją pracę to wybrałabym: ciągły rozwój, współpraca między
              pracownikami oraz satysfakcja. Czym Norauto wyróżnia się na tle konkurencji? Jest firmą przyjazną
              dla klientów, nastawioną na tworzenie i proponowanie nowych rozwiązań, ułatwiających życie
              kierowcom. <br /> <br />
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-6.jpg'
									alt='Iza'/>
					</div>
				</li>

				<li class='testimonial-item'>
					<p class='testimonial-title'>NORAUTO DAJE STABILNOŚĆ ZATRUDNIENIA</p>
					<div class='testimonial-meta'>
						<div class='testimonial-details'>
							<p class='testimonial-details-title'>Serwis</p>
							<p class='testimonial-details-name'>Dawid</p>
							<p class='testimonial-details-misc'>W Norauto 16 lat i 6 miesięcy</p>
						</div>
						<div class='testimonial-quote'>
              Pracując w Norauto doceniam panującą tutaj atmosferę. To spora dawka zadowolenia i przyjemności.
              Mam duże poczucie bezpieczeństwa, bo Norauto daje stabilność zatrudnienia, a jednocześnie
              motywuje do rozwoju zawodowego, poszerzania swojej wiedzy i samodzielności na zajmowanym
              stanowisku. <br/> <br/> <br/>
						</div>
					</div>
					<div class='testimonial-image'>
						<img class='testimonial-img' src='<?php echo get_template_directory_uri(); ?>/assets/img/testimonials/testimonial-7.jpg'
									alt='Dawid'/>
					</div>
				</li>

			</ul>
			<div class='testimonial-controls'>
				<button class='testimonial-control previous homePrev'>
					<svg
						xmlns='http://www.w3.org/2000/svg'
						viewBox='0 0 69.8 67.9'
					>
						<path
							fill='#fcaf17'
							d='M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z'
						/>
					</svg>
				</button>
				<button class='testimonial-control next homeNext'>
					<svg
						xmlns='http://www.w3.org/2000/svg'
						viewBox='0 0 69.8 67.9'
					>
						<path
							fill='#fcaf17'
							d='M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z'
						/>
					</svg>
				</button>
			</div>
		</div>
	</div>
	<div class='benefits'>
		<div class='benefits-wrapper'>
			<div class='benefits-intro'>
				<h4 class='benefits-header'>
					Benefity
				</h4>
				<p class='benefits-subheader'>
					W Norauto dbamy o <strong>zadowolenie naszych Pracowników.</strong>
				</p>
				<p class='benefits-slider-intro'>
					Oprócz wynagrodzenia zyskujesz:
				</p>
			</div>
			<div class='benefit-slider-wrapper'>
				<ul class='benefit-slider'>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/uop_c1.svg'
								alt='Umowa o pracę i stabilizacja'
							/>
							<p class='benefit-title'>
								Umowa o pracę i stabilizacja
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/pewnosc_c1.svg'
								alt='Pewność zatrudnienia'
							/>
							<p class='benefit-title'>
								Pewność zatrudnienia
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/atmosfera_c1.svg'
								alt='Przyjazna atmosfera pracy'
							/>
							<p class='benefit-title'>
								Przyjazna atmosfera pracy
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/przelozony_c1.svg'
								alt='Przełożony, który jest blisko swoich pracowników'
							/>
							<p class='benefit-title'>
								Przełożony, który jest blisko swoich pracowników
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/opieka_c1.svg'
								alt='Prywatna opieka medyczna'
							/>
							<p class='benefit-title'>
								Prywatna opieka medyczna
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/ubezpieczenie_c1.svg'
								alt='Ubezpieczenie na życie'
							/>
							<p class='benefit-title'>
								Ubezpieczenie na życie
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/sport_c1.svg'
								alt='Abonament sportowy'
							/>
							<p class='benefit-title'>
								Abonament sportowy
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/swiadczenia_c1.svg'
								alt='Świadczenia z Funduszu Socjalnego'
							/>
							<p class='benefit-title'>
								Świadczenia z Funduszu Socjalnego
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/rozwoj_c1.svg'
								alt='Możliwość rozwoju zawodowego'
							/>
							<p class='benefit-title'>
								Możliwość rozwoju zawodowego
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/szkolenia_c1.svg'
								alt='Szeroki wachlarz szkoleń'
							/>
							<p class='benefit-title'>
								Szeroki wachlarz szkoleń
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/projekty_c1.svg'
								alt='Udział w wyjątkowych projektach'
							/>
							<p class='benefit-title'>
								Udział w wyjątkowych projektach
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/blisko_domu_c1.svg'
								alt='Gęsta sieć oddziałów'
							/>
							<p class='benefit-title'>
								Gęsta sieć oddziałów
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/spotkania_integracyjne_c1.svg'
								alt='Spotkania integracyjne'
							/>
							<p class='benefit-title'>
								Spotkania integracyjne
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/bonus_polecenie_c1.svg'
								alt='Bonus za polecenie pracownika'
							/>
							<p class='benefit-title'>
								Bonus za polecenie pracownika
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/zakres_samodzielnosci_c1.svg'
								alt='Duży zakres samodzielności'
							/>
							<p class='benefit-title'>
								Duży zakres samodzielności
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/nauka_jezykow_c1.svg'
								alt='Nauka języków obcych'
							/>
							<p class='benefit-title'>
								Nauka języków obcych
							</p>
						</div>
					</li>
					<li class='benefit-item'>
						<div class='benefit-item-wrapper'>
							<img
								class='benefit-icon'
								src='<?php echo get_template_directory_uri(); ?>/assets/img/benefits/premie_c1.svg'
								alt='Premie za realizację celów, na które masz wpływ'
							/>
							<p class='benefit-title'>
								Premie za realizację celów, na które masz wpływ
							</p>
						</div>
					</li>
				</ul>
			</div>
			<div class='benefit-slider-controls'>
				<button class='benefit-slider-btn prev benefitsPrev'>
					<svg
						xmlns='http://www.w3.org/2000/svg'
						viewBox='0 0 69.8 67.9'
					>
						<path
							fill='#fff'
							d='M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z'
						/>
					</svg>
				</button>
				<button class='benefit-slider-btn next benefitsNext'>
					<svg
						xmlns='http://www.w3.org/2000/svg'
						viewBox='0 0 69.8 67.9'
					>
						<path
							fill='#fff'
							d='M31.6 8.7l3.1-3.1c.7-.7 1.5-1 2.4-1 .9 0 1.7.3 2.4 1l27.3 27.3c.7.7 1 1.5 1 2.4 0 .9-.3 1.7-1 2.4L39.5 64.9c-.7.7-1.5 1-2.4 1-.9 0-1.7-.3-2.4-1l-3.1-3.1c-.8-.7-1.1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.4l16.9-16.2H8.1c-.9 0-1.7-.3-2.4-1s-1-1.5-1-2.4V33c0-.9.3-1.7 1-2.4s1.5-1 2.4-1h40.4L31.6 13.4c-.7-.7-1-1.5-1.1-2.4 0-.9.4-1.7 1.1-2.3z'
						/>
					</svg>
				</button>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
