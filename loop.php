<div class="shop-content">
	<ul class="job-offers-container">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<?php
			$miejscepracy = get_post_meta($post->ID, 'wpcf-miejsce-pracy', true);
			$deepest_category = wp_get_object_terms( $post->ID, 'category', array('orderby' => 'term_order', 'order' => 'ASC', 'fields' => 'all'));
		?>

		<li class="job-offer-item">
			<a href="<?php the_permalink(); ?>" class="job-offer-details">
				<h3 class="job-offer-title">
					<?php the_title(); ?>
				</h3>
				<div class="job-offer-meta">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pin.svg" class="job-offer-icon" alt="Lokalizacja" />
					<p class="job-offer-location">
						<span class="job-offer-address">
							<?php echo $miejscepracy; ?>
						</span>
						<span class="job-offer-town">
							<?php echo $deepest_category[2]->name; ?>
						</span>
					</p>
				</div>
				<button class="job-offer-btn">
					Zobacz ofertę
				</button>
			</a>
		</li>

		<?php endwhile; ?>
		<?php else : ?>
	</ul>
</div>

<!-- Empty State -->
<div class="job-search-empty-state">
	<h4 class="empty-state-text">
		Przepraszamy, nie znaleziono żadnych wyników.
	</h4>
</div>
<?php endif; ?>
