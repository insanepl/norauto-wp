<?php /* Template Name: Success Template */ get_header(); ?>
<?php $status = $_GET['success']; ?>
<div class="content-container">
	<div class="homepage-hero">
		<div class="hero-container is-offer sent">
			<div class="hero-image is-offer sent">
				<h1 class="hero-header">
					<?php
						if ($status == 'true') {
							echo 'APLIKACJA WYSŁANA';
						} else {
							echo 'BŁĄD';
						}
					?>
				</h1>
				<p class="sent-sub">
					<?php
						if ($status == 'true') {
							echo '';
						} else {
							echo 'Niestety, nie udało się wysłać Twojej aplikacji, spróbuj ponownie później';
						}
					?>
				</p>
			</div>
		</div>
	</div>
	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div>
<?php get_footer(); ?>
