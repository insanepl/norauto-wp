<?php /* Template Name: Our Values Template */ get_header(); ?>
<div class="content-container">
  <div class="about-us-hero">
    <div class="about-us-vid-wrapper">
      <video autoplay muted loop class="about-us-vid">
        <source src="<?php echo get_template_directory_uri(); ?>/assets/vid/nasze_wartosci.ogg" type="video/ogg" />
        <source src="<?php echo get_template_directory_uri(); ?>/assets/vid/nasze_wartosci.mp4" type="video/mp4" />
      </video>
      <div class="about-us-more-wrapper">
        <button class="see-more-btn video-modal-btn is-about-us">Zobacz cały film</button>
      </div>
    </div>
    <div class="about-us-content-wrapper">
      <h1 class="about-us-title">
        Kultura
        <span>4 kółek</span>
      </h1>
      <p class="about-us-text">
        W norauto kierujemy się wartościami, które stosujemy w codziennej pracy.
				<br />
        To dzięki nim rozwijamy się tak, jak chcemy i w wyznaczonym przez nas kierunku.
      </p>
    </div>
  </div>
  <div class="about-us-section">
    <ul class="about-us-accordion">
      <li class="about-us-accordion-element is-active">
        <span class="about-us-accordion-btn">Dzielimy się</span>
        <div class="about-us-element-content">
          <h2 class="about-us-accordion-title">
            Zbieramy doświadczenie, dzielimy się zdobytą  
            <strong>
              wiedzą i pasją.
            </strong>
          </h2>
          <p class="about-us-accordion-text">
            Pracując w Norauto masz realny wpływ na rozwój naszej organizacji. <br /> <br />
            Angażujemy naszych pracowników w kształtowanie strategii, oferty handlowej i wybór benefitów. Zachęcamy ich do zgłaszania swoich pomysłów i usprawnień, które pomogą nie tylko nam, ale także naszym klientom. <br /> <br />
            Masz pomysł? Zgłoś go! Potrafimy słuchać i jesteśmy otwarci na opinie każdego członka zespołu.
          </p>
        </div>
      </li>
      <li class="about-us-accordion-element">
        <span class="about-us-accordion-btn">Okazujemy szacunek</span>
        <div class="about-us-element-content">
          <h2 class="about-us-accordion-title">
            Stawiamy na <strong>szacunek i zaufanie</strong> na wszystkich poziomach. 
          </h2>
          <p class="about-us-accordion-text">
            Nieważne, czy dopiero zaczynasz pracę w Norauto lub może jesteś z nami od kilku lat, jesteś osobą młodą czy po sześćdziesiątce, kobietą lub mężczyzną. <br /> <br />
            Szacunek oznacza dla nas również jasne reguły gry i przejrzyste zasady współpracy między wszystkimi pracownikami. <br /> <br />
            Dobra atmosfera wyróżnia nas jako pracodawcę, dzięki czemu tworzymy miejsce pracy, do którego przychodzi się z przyjemnością.
          </p>
        </div>
      </li>
      <li class="about-us-accordion-element">
        <span class="about-us-accordion-btn">Rozwijamy się</span>
        <div class="about-us-element-content">
          <h2 class="about-us-accordion-title">
            <strong>Rozwój</strong> każdego pracownika jest naszym priorytetem.
          </h2>
          <p class="about-us-accordion-text">
            Wierzymy, że z pozoru drobne działania wpływają na sukces całej firmy. <br /> <br />
            W Norauto masz pewność, że nie będziesz stać w miejscu. Bierz udział w szkoleniach, ubiegaj się o awans i wrzuć karierę na wyższy bieg! Aż 95 % osób na stanowiskach kierowniczych w naszej firmie zaczynało swoją podróż w Norauto od niższego stanowiska.
          </p>
        </div>
      </li>
      <li class="about-us-accordion-element">
        <span class="about-us-accordion-btn">Cenimy przedsiębiorczość</span>
        <div class="about-us-element-content">
          <h2 class="about-us-accordion-title">
            W Norauto cenimy zaangażowanie i doceniamy zaradność każdego z pracowników - <strong>wierzymy, że pracując w zespole nie ma rzeczy niemożliwych!</strong>
          </h2>
          <p class="about-us-accordion-text">
            Każdy dzień przynosi nowe wyzwania. Coś, co dla jednych jest problemem, dla nas stanowi okazję do wykazania się pomysłowością i inicjatywą. 
          </p>
        </div>
      </li>
      <li class="about-us-accordion-element">
        <span class="about-us-accordion-btn">Stawiamy na autentyczność</span>
        <div class="about-us-element-content">
          <h2 class="about-us-accordion-title">
            <strong>Norauto skupia ludzi z pasją,</strong> lubiących to co robią, pełnych pozytywnej energii i zapału w codziennej pracy. 
          </h2>
          <p class="about-us-accordion-text">
            Dążymy do tego, aby nasze relacje były szczere, oparte na uczciwości i wzajemnym zaufaniu.
          </p>
        </div>
      </li>
    </ul>
  </div>
</div>
<div class="video-modal">
  <button class="close-video-modal">&times;</button>
</div>
<?php get_footer(); ?>
