      <div class="site-container">
        <footer class="section footer-section">
          <div class="container footer-container">
            <div class="footer-elements-wrapper">
              <div class="social-wrapper">
                <a href="https://www.facebook.com/norautoPL/?ref=br_rs" target="_blank" rel="noopener noreferrer">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.6 829.6">
                    <path d="M829.6 783.3c0 12.3-4.3 23.1-13 32.4s-19.8 13.9-33.3 13.9H572.2V507.4h107.4l16.7-124.1H572.2v-79.6q0-31.5 11.1-44.4c9.9-11.1 26.5-16.7 50-16.7H700v-113q-38.85-3.75-96.3-3.7-72.15 0-116.7 42.6-44.4 42.6-44.4 122.2v92.6H335.2v124.1h107.4v322.2H46.3c-13.6 0-24.7-4.6-33.3-13.9S0 795.6 0 783.3v-737C0 32.7 4.3 21.6 13 13S32.8 0 46.3 0h737c12.3 0 23.1 4.3 32.4 13s13.9 19.8 13.9 33.3v737z"/>
                  </svg>
                </a>
                <a href="https://www.linkedin.com/company/norautopolska/" target="_blank" rel="noopener noreferrer">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 833 833">
                    <path d="M773.5 0c16.1 0 30.1 5.9 41.8 17.7S833 43.4 833 59.5v714c0 16.1-5.9 30.1-17.7 41.8S789.6 833 773.5 833h-714c-16.1 0-30.1-5.9-41.8-17.7S0 789.6 0 773.5v-714c0-16.1 5.9-30.1 17.7-41.8S43.4 0 59.5 0zM189.6 262.1c19.8 0 36.9-7.1 51.1-21.4s21.4-31 21.4-50.2-7.1-35.9-21.4-50.2-31.3-21.4-51.1-21.4-36.6 7.1-50.2 21.4-20.5 31-20.5 50.2 6.8 35.9 20.5 50.2 30.4 21.4 50.2 21.4zM251 714h1.9V316.1H128.3V714zm463 0V496.4q0-93-26-135.7c-23.6-35.9-63.8-53.9-120.9-53.9q-42.75 0-76.2 20.5c-18.6 11.2-32.2 25.4-40.9 42.8h-1.9v-53.9h-119v397.9h122.7V516.9c0-31 4.3-54.5 13-70.7q18.6-31.65 61.4-31.6t55.8 35.3c6.2 14.9 9.3 38.4 9.3 70.7V714H714z"/>
                  </svg>
                </a>
              </div>
              <div class="footer-box job-wrapper">
                <h2 class="footer-heading">Szukasz pracy?</h2>
                <div class="contact-wrapper">
                  <a href="mailto:praca@norauto.pl" class="contact-credentials">praca@norauto.pl</a>
                </div>
              </div>
              <div class="footer-box school-wrapper">
                <h2 class="footer-heading">Reprezentujesz szkołę?</h2>
                <div class="contact-wrapper">
                  <a href="tel:+48791331732" class="contact-credentials">+48 791 331 732</a>
                  <a href="mailto:praktyki@norauto.pl" class="contact-credentials">praktyki@norauto.pl</a>
                </div>
              </div>
            </div>
            <div class="terms-wrapper">
              <a href="https://www.norauto.pl/polityka-prywatnosci" target="_blank" rel="noopener noreferrer">Polityka prywatności</a>
              <a href="https://www.norauto.pl/informacje-cookies" target="_blank" rel="noopener noreferrer">Informacje cookies</a>
            </div>
          </div>
        </footer>
      </div>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/objectFitPolyfill@2.1.1/dist/objectFitPolyfill.min.js" integrity="sha256-XW8nncDEhg9CZZuBoFKTcCq5eTmsSoqRt8ItyeUG308=" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.12/jquery.transit.min.js" integrity="sha256-rqEXy4JTnKZom8mLVQpvni3QHbynfjPmPxQVsPZgmJY=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha256-NXRS8qVcmZ3dOv3LziwznUHPegFhPZ1F/4inU7uC8h0=" crossorigin="anonymous"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.min.js"></script>
		<?php wp_footer(); ?>
	</body>
</html>
