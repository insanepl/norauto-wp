<?php /* Template Name: Internship Template */ get_header(); ?>
<div class="content-container spontanious-app has-modal">
  <div class="internship-hero-section">
    <div class="container">
      <div class="internship-hero-wrapper">
        <h1 class="internship-title">
          Praktyki i staże
        </h1>
        <h2 class="internship-subtitle">
          Praktyki
        </h2>
        <h2 class="internship-subtitle">
          Pracownicy młodociani
        </h2>
        <h2 class="internship-subtitle">
          Płatny staż
        </h2>
      </div>
    </div>
  </div>
  <div class="internship-intro-section">
    <div class="container">
      <div class="internship-intro-wrapper">
        <div class="intro-title">
          Norauto
          <strong>Jazda testowa dla początkujących</strong>
        </div>
        <p class="intro-text">
          Uczysz się? Norauto jest dla Ciebie! <br />
           Współpracujemy ze szkołami o profilach handlowych i samochodowych.
          Dostrzegamy potencjał tkwiący w młodych ludziach,
          dlatego prowadzimy programy praktyk, staży lub elastycznej pracy dla uczniów.
        </p>
      </div>
    </div>
  </div>
  <div class="internship-accordion-section">
    <ul class="internship-accordion">
      <li class="accordion-element is-active">
        <div class="accordion-title-wrapper is-internship">
          <h3 class="accordion-title">Praktyki</h3>
        </div>
        <div class="accordion-content">
          <div class="accordion-image-wrapper">
            <img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/foto-praktyki.jpg"
              alt="Praktyki"
              class="accordion-image"
            />
          </div>
          <div class="accordion-content-wrapper">
            <p class="accordion-text">
              Jesteś uczniem szkoły branżowej, technikum o profilu samochodowym lub
              handlowym? Jeśli tak, dołącz do Norauto i zdobądź pierwsze doświadczenie!
              Myślisz o zawodzie Mechanika samochodowego? <strong>Możesz odbyć praktyki w jednym
              z naszych 39 serwisów.</strong> Jesteś uczniem szkoły o profilu handlowym i interesujesz
              się motoryzacją? <strong>Możesz odbyć praktyki jako Sprzedawca w jednym z naszych
              sklepów na terenie całej Polski!</strong>
            </p>
            <h3 class="accordion-heading">
              Możesz liczyć na zdobycie nowych kompetencji, własny rozwój, wsparcie ze strony
              opiekuna, praktyki w świetnej atmosferze i <strong>realną szansę na stałą pracę!</strong>
            </h3>
            <p class="accordion-text">
              Praktyki organizujemy przez cały rok, mogą one trwać miesiąc, jeden lub dwa
              semestry. Jesteśmy elastyczni, dlatego to Ty decydujesz, jak długo chcesz brać w
              nich udział.
            </p>
            <div class="accordion-footer">
              <h3 class="accordion-btn-heading">Jesteś zainteresowany?</h3>
              <button class="accordion-btn">Skontaktuj się z nami!</button>
            </div>
          </div>
        </div>
      </li>
      <li class="accordion-element">
        <div class="accordion-title-wrapper is-employee">
          <h3 class="accordion-title">Pracownicy młodociani</h3>
        </div>
        <div class="accordion-content">
          <div class="accordion-image-wrapper">
            <img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/foto-pracownicy-mlodociani.jpg"
              alt="Mlodociani pracownicy"
              class="accordion-image"
            />
          </div>
          <div class="accordion-content-wrapper">
            <p class="accordion-text">
              To oferta dla uczniów szkół o profilu samochodowym, które podlegają pod OHP,
              Izbę Rzemieślniczą lub Cech. Norauto jest miejscem, w którym możesz nauczyć się
              zawodu jako pracownik młodociany.
            </p>
            <h3 class="accordion-heading">
              Jako nasz pracownik możesz <strong>łączyć pracę z nauką</strong>, a dodatkowo zyskujesz prawo
              do <strong>urlopu</strong>, tych samych <strong>benefitów</strong> co inni pracownicy (opieka medyczna, karta
              Multisport, ubezpieczenie na życie), oraz co najważniejsze - <strong>zarabiasz!</strong>
            </h3>
            <div class="accordion-footer">
              <h3 class="accordion-btn-heading">Jesteś zainteresowany?</h3>
              <button class="accordion-btn">Skontaktuj się z nami!</button>
            </div>
          </div>
        </div>
      </li>
      <li class="accordion-element">
        <div class="accordion-title-wrapper is-training">
          <h3 class="accordion-title">Płatny staż</h3>
        </div>
        <div class="accordion-content">
          <div class="accordion-image-wrapper">
            <img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/foto-platny-staz.jpg"
              alt="Płatny staż"
              class="accordion-image is-internship"
            />
          </div>
          <div class="accordion-content-wrapper">
            <p class="accordion-text">
              Chcesz zarobić na wakacje? Nasz program stażowy to najlepsze rozwiązanie dla
              tych, którzy mają już za sobą pierwsze doświadczenie w pracy w serwisie lub
              sklepie. Dostosujemy się do Twoich potrzeb, dlatego w ramach stażu możesz
              pracować u nas po szkole, w weekendy lub w wakacje.
            </p>
            <h3 class="accordion-heading">
              <strong>Dołącz do nas</strong>, a zarobione pieniądze wydaj na co chcesz!
            </h3>
            <div class="accordion-footer">
              <h3 class="accordion-btn-heading">Jesteś zainteresowany?</h3>
              <button class="accordion-btn">Skontaktuj się z nami!</button>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
	<?php get_template_part('template-modal'); ?>
</div>
<?php get_footer(); ?>
