<?php get_header(); ?>
<?php $fraza = $_GET['s']; $cat = $_GET['cat']; $position = $_GET['position']; ?>

<div class="content-container">
	<div class="shop-header is-centers">
		<h2 class="shop-heading">
      <span class="shop-heading-part bold">
        Centrala
      </span>
			<span class="shop-heading-part normal">
        Oferty pracy
      </span>
		</h2>
	</div>

	<div class="job-offers-search-wrapper">
		<?php get_template_part( 'searchform' ); ?>
	</div>

	<?php
		if (empty($fraza)) {
			$args = array(
				'post_type' => 'oferta-pracy',
				'cat' => $cat,
				'meta_key' => 'wpcf-poziom-stanowiska',
				'meta_value' => $position,
				'category__not_in' => 4
			);

			$the_query = new WP_Query( $args );
	?>
	<div class="shop-content">
		<ul class="job-offers-container">
	<?php
			if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
				$miejscepracy = get_post_meta($post->ID, 'wpcf-miejsce-pracy', true);
				$deepest_category = wp_get_object_terms( $post->ID, 'category', array('orderby' => 'term_order', 'order' => 'ASC', 'fields' => 'all'));
	?>
			<li class="job-offer-item">
				<a href="<?php the_permalink(); ?>" class="job-offer-details">
					<h3 class="job-offer-title">
						<?php the_title(); ?>
					</h3>
					<div class="job-offer-meta">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pin.svg" class="job-offer-icon" alt="Lokalizacja" />
						<p class="job-offer-location">
						<span class="job-offer-address">
							<?php echo $miejscepracy; ?>
						</span>
							<span class="job-offer-town">
							<?php echo $deepest_category[2]->name; ?>
						</span>
						</p>
					</div>
					<button class="job-offer-btn">
						Zobacz ofertę
					</button>
				</a>
			</li>

	<?php
			endwhile;
			wp_reset_postdata();
			endif;

		} else {
			get_template_part( 'loop' );
		}
	?>
		</ul>
	</div>
</div>
<div class="shop-banner">
	<div class="banner-content">
		<p class="banner-heading">
        <span>
          NIE ZNALAZŁEŚ
        </span>
				<span>
          PASUJĄCEJ OFERTY?
        </span>
		</p>
		<p class="banner-subhead">
			Jeśli nie znalazłeś oferty, która spełnia Twoje oczekiwania, prześlij nam swoją aplikację.
			<br /><br />
			Jeżeli znajdziemy stanowisko odpowiadające Twojemu doświadczeniu i oczekiwaniom - skontaktujemy się z Tobą.
		</p>
	</div>
</div>

<?php // get_template_part( 'pagination' ); ?>
<?php get_footer(); ?>
